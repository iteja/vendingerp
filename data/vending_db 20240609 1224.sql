﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 09/06/2024 12:24:07
-- Server version: 5.5.5-10.1.40-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS vending_db;

CREATE DATABASE IF NOT EXISTS vending_db
CHARACTER SET utf8
COLLATE utf8_general_ci;

--
-- Set default database
--
USE vending_db;

--
-- Create table `empresas`
--
CREATE TABLE IF NOT EXISTS empresas (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) NOT NULL,
  industria varchar(255) DEFAULT NULL,
  ciudad varchar(255) DEFAULT NULL,
  coordenadas varchar(255) DEFAULT NULL,
  direccion varchar(255) DEFAULT NULL,
  representante varchar(255) DEFAULT NULL,
  telefono varchar(50) DEFAULT NULL,
  correo varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 45,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `maquinas`
--
CREATE TABLE IF NOT EXISTS maquinas (
  id int(11) NOT NULL AUTO_INCREMENT,
  idEmpresas int(11) DEFAULT NULL,
  numero_de_serie varchar(255) NOT NULL,
  nombre varchar(255) NOT NULL,
  tipo_de_maquina varchar(255) DEFAULT NULL,
  estado tinyint(1) NOT NULL DEFAULT 0,
  fecha_de_ultima_instalacion date DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 49,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `numero_de_serie` on table `maquinas`
--
ALTER TABLE maquinas
ADD UNIQUE INDEX numero_de_serie (numero_de_serie);

--
-- Create foreign key
--
ALTER TABLE maquinas
ADD CONSTRAINT maquinas_ibfk_1 FOREIGN KEY (idEmpresas)
REFERENCES empresas (id) ON DELETE CASCADE;

--
-- Create table `mantenimientos`
--
CREATE TABLE IF NOT EXISTS mantenimientos (
  id int(11) NOT NULL AUTO_INCREMENT,
  idMaquinas int(11) NOT NULL,
  descripcion text DEFAULT NULL,
  estado tinyint(1) NOT NULL DEFAULT 0,
  fecha date NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 6,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE mantenimientos
ADD CONSTRAINT mantenimientos_ibfk_1 FOREIGN KEY (idMaquinas)
REFERENCES maquinas (id) ON DELETE CASCADE;

--
-- Create table `incidencias`
--
CREATE TABLE IF NOT EXISTS incidencias (
  id int(11) NOT NULL AUTO_INCREMENT,
  idMaquinas int(11) NOT NULL,
  descripcion text NOT NULL,
  fecha date NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 5,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE incidencias
ADD CONSTRAINT incidencias_ibfk_1 FOREIGN KEY (idMaquinas)
REFERENCES maquinas (id) ON DELETE CASCADE;

--
-- Create table `piezas_reemplazadas`
--
CREATE TABLE IF NOT EXISTS piezas_reemplazadas (
  idIncidencias int(11) NOT NULL,
  piezas_reemplazadas varchar(255) NOT NULL,
  PRIMARY KEY (idIncidencias, piezas_reemplazadas)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE piezas_reemplazadas
ADD CONSTRAINT piezas_reemplazadas_ibfk_1 FOREIGN KEY (idIncidencias)
REFERENCES incidencias (id) ON DELETE CASCADE;

--
-- Create table `contratos`
--
CREATE TABLE IF NOT EXISTS contratos (
  id int(11) NOT NULL AUTO_INCREMENT,
  idEmpresas int(11) NOT NULL,
  idMaquinas int(11) NOT NULL,
  tipo varchar(255) NOT NULL,
  descripcion text DEFAULT NULL,
  fecha_de_inicio date NOT NULL,
  fecha_fin date NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 22,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE contratos
ADD CONSTRAINT contratos_ibfk_1 FOREIGN KEY (idEmpresas)
REFERENCES empresas (id) ON DELETE CASCADE;

--
-- Create foreign key
--
ALTER TABLE contratos
ADD CONSTRAINT contratos_ibfk_2 FOREIGN KEY (idMaquinas)
REFERENCES maquinas (id) ON DELETE CASCADE;

--
-- Create table `rutas`
--
CREATE TABLE IF NOT EXISTS rutas (
  id int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(255) NOT NULL,
  descripcion text DEFAULT NULL,
  duracion time NOT NULL,
  distancia_km decimal(10, 2) NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 42,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create table `empresas_rutas`
--
CREATE TABLE IF NOT EXISTS empresas_rutas (
  idEmpresas int(11) NOT NULL,
  idRutas int(11) NOT NULL,
  PRIMARY KEY (idEmpresas, idRutas)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE empresas_rutas
ADD CONSTRAINT empresas_rutas_ibfk_1 FOREIGN KEY (idEmpresas)
REFERENCES empresas (id) ON DELETE CASCADE;

--
-- Create foreign key
--
ALTER TABLE empresas_rutas
ADD CONSTRAINT empresas_rutas_ibfk_2 FOREIGN KEY (idRutas)
REFERENCES rutas (id) ON DELETE CASCADE;

--
-- Create table `productos`
--
CREATE TABLE IF NOT EXISTS productos (
  id int(11) NOT NULL AUTO_INCREMENT,
  codigo_barras varchar(255) NOT NULL,
  nombre varchar(255) NOT NULL,
  descripcion text DEFAULT NULL,
  categoria varchar(255) DEFAULT NULL,
  stock int(11) NOT NULL DEFAULT 0,
  context varchar(50) DEFAULT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 192,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create index `codigo_barras` on table `productos`
--
ALTER TABLE productos
ADD UNIQUE INDEX codigo_barras (codigo_barras);

--
-- Create table `ventas`
--
CREATE TABLE IF NOT EXISTS ventas (
  id int(11) NOT NULL AUTO_INCREMENT,
  idProductos int(11) NOT NULL,
  idMaquinas int(11) NOT NULL,
  metodo_de_pago varchar(255) NOT NULL,
  precio decimal(10, 2) NOT NULL,
  cantidad int(11) NOT NULL,
  fecha date NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE ventas
ADD CONSTRAINT ventas_ibfk_1 FOREIGN KEY (idProductos)
REFERENCES productos (id) ON DELETE CASCADE;

--
-- Create foreign key
--
ALTER TABLE ventas
ADD CONSTRAINT ventas_ibfk_2 FOREIGN KEY (idMaquinas)
REFERENCES maquinas (id) ON DELETE CASCADE;

--
-- Create table `maquinas_productos`
--
CREATE TABLE IF NOT EXISTS maquinas_productos (
  idMaquinas int(11) NOT NULL,
  idProductos int(11) NOT NULL,
  stock int(11) DEFAULT 0,
  PRIMARY KEY (idMaquinas, idProductos)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 1820,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE maquinas_productos
ADD CONSTRAINT maquinas_productos_ibfk_1 FOREIGN KEY (idMaquinas)
REFERENCES maquinas (id) ON DELETE CASCADE;

--
-- Create foreign key
--
ALTER TABLE maquinas_productos
ADD CONSTRAINT maquinas_productos_ibfk_2 FOREIGN KEY (idProductos)
REFERENCES productos (id) ON DELETE CASCADE;

--
-- Create table `compras`
--
CREATE TABLE IF NOT EXISTS compras (
  id int(11) NOT NULL AUTO_INCREMENT,
  idProductos int(11) NOT NULL,
  proveedor varchar(255) NOT NULL,
  cantidad int(11) NOT NULL,
  precio decimal(10, 2) NOT NULL,
  fecha date NOT NULL,
  PRIMARY KEY (id)
)
ENGINE = INNODB,
AUTO_INCREMENT = 177,
AVG_ROW_LENGTH = 5461,
CHARACTER SET utf8,
COLLATE utf8_general_ci;

--
-- Create foreign key
--
ALTER TABLE compras
ADD CONSTRAINT compras_ibfk_1 FOREIGN KEY (idProductos)
REFERENCES productos (id) ON DELETE CASCADE;

-- 
-- Dumping data for table empresas
--
INSERT INTO empresas VALUES
(39, 'El Corte Inglés', 'Distribución', 'Santander', '43.4375, -3.8398', 'Nueva Montaña', 'Marta Álvarez', '633723923', 'malvarez@elcorteingles.es'),
(40, 'HOTEL BED4U', 'Hotelería', 'Santander', '43.4524, -3.8282', 'Av. Parayas, 2a', 'Lucía Pérez', '555327821', ''),
(43, 'Universidad de Cantabria (UC)', '', 'Santander', '43.47235, -3.80191', 'Av. de los Castros', 'José Manuel Canales', '942764278', ''),
(44, 'Eurostars Hotel Real', 'Hotelería', 'Santander', '43.46650, -3.78207', 'Av. Pérez Galdós 28', 'Pablo Rodrigo', '', '');

-- 
-- Dumping data for table maquinas
--
INSERT INTO maquinas VALUES
(44, NULL, 'GH78UIL9OS', 'SnackMaster 2000', 'Snacks', 0, NULL),
(45, NULL, 'JFD984KL228', 'CoolDrinks Deluxe', 'Bebidas Frías', 0, NULL),
(46, NULL, 'PNC53RFUL7', 'CaféExpress Pro', 'Café', 0, NULL),
(47, NULL, 'UT567SAW32', 'SweetTooth Paradise', 'Dulces', 0, NULL),
(48, NULL, '77UISD56JJ', 'HotCup Master', 'Bebidas Calientes', 0, NULL);

-- 
-- Dumping data for table incidencias
--
INSERT INTO incidencias VALUES
(1, 44, '', '2024-06-07'),
(2, 47, '', '2024-06-04'),
(3, 48, '', '2024-06-04'),
(4, 45, '', '2024-06-06');

-- 
-- Dumping data for table rutas
--
INSERT INTO rutas VALUES
(40, 'Ruta 1', '', '00:00:00', 0.00),
(41, 'Ruta 2', 'Ruta de hoteles y universidad', '00:00:00', 0.00);

-- 
-- Dumping data for table productos
--
INSERT INTO productos VALUES
(144, '8410069001009', 'Barrita de Granola', 'Barrita de granola con chocolate y frutos secos.', 'Snacks', 40, NULL),
(145, '7501031311303', 'Papas Fritas', 'Bolsa de papas fritas crujientes y saladas.', 'Snacks', 20, NULL),
(146, '8412345678905', 'Galletas de Chocolate', 'Galletas de chocolate con trozos de chocolate.', 'Snacks', 10, NULL),
(147, '7502000000013', 'Trail Mix', 'Mezcla de frutos secos y pasas.', 'Snacks', 15, NULL),
(148, '7791520072100', 'Palitos de Pretzel', 'Palitos de pretzel salados.', 'Snacks', 30, NULL),
(149, '7501011505201', 'Popcorn Salado', 'Palomitas de maíz saladas.', 'Snacks', 20, NULL),
(150, '7501025415207', 'Chocolate Amargo', 'Tableta de chocolate amargo.', 'Snacks', 25, NULL),
(151, '7501710600035', 'Bolsa de Frutos Secos', 'Mezcla de frutos secos variados.', 'Snacks', 40, NULL),
(152, '7501825306301', 'Chocolatina de Caramelo', 'Chocolatina rellena de caramelo.', 'Snacks', 50, NULL),
(153, '7501759600044', 'Crackers de Queso', 'Galletas crackers con sabor a queso.', 'Snacks', 10, NULL),
(154, '8410069021009', 'Coca-Cola', 'Refresco de cola frío en lata.', 'Bebidas Frías', 35, NULL),
(155, '7501031341303', 'Sprite', 'Refresco de limón frío en lata.', 'Bebidas Frías', 15, NULL),
(156, '8412343678905', 'Agua Mineral', 'Botella de agua mineral sin gas.', 'Bebidas Frías', 25, NULL),
(157, '7502003000013', 'Fanta Naranja', 'Refresco de naranja frío en lata.', 'Bebidas Frías', 20, NULL),
(158, '7791320072100', 'Nestea', 'Té helado sabor a limón en botella.', 'Bebidas Frías', 30, NULL),
(159, '7501013505201', 'Red Bull', 'Bebida energética en lata.', 'Bebidas Frías', 45, NULL),
(160, '7501023415207', 'Pepsi', 'Refresco de cola frío en lata.', 'Bebidas Frías', 35, NULL),
(161, '7501713600035', 'Ice Tea', 'Té helado sabor a durazno en botella.', 'Bebidas Frías', 20, NULL),
(162, '7501823306301', 'Schweppes Tónica', 'Agua tónica fría en lata.', 'Bebidas Frías', 10, NULL),
(163, '7501753600044', 'Monster Energy', 'Bebida energética en lata.', 'Bebidas Frías', 15, NULL),
(164, '8430069001009', 'Espresso', 'Café espresso fuerte y aromático.', 'Café', 30, NULL),
(165, '7531031311303', 'Latte', 'Café latte cremoso con espuma de leche.', 'Café', 20, NULL),
(166, '8312345678905', 'Capuccino', 'Café capuccino con una capa de espuma de leche.', 'Café', 25, NULL),
(167, '7302000000013', 'Mocha', 'Café mocha con chocolate y crema batida.', 'Café', 40, NULL),
(168, '7731520072100', 'Café Americano', 'Taza de café americano caliente.', 'Café', 50, NULL),
(169, '7531011505201', 'Café con Leche', 'Taza de café con leche caliente.', 'Café', 10, NULL),
(170, '7531025415207', 'Macchiato', 'Café macchiato con una pizca de leche.', 'Café', 35, NULL),
(171, '7531710600035', 'Café Vienés', 'Café vienés con nata montada y chocolate rallado.', 'Café', 15, NULL),
(172, '8420069001009', 'Chocolate con Leche', 'Tableta de chocolate con leche.', 'Chocolates', 25, NULL),
(173, '7591031311303', 'Gomitas de Frutas', 'Gomitas de frutas surtidas.', 'Chucherías', 20, NULL),
(174, '8422345678905', 'Malvaviscos', 'Malvaviscos dulces y esponjosos.', 'Chucherías', 30, NULL),
(175, '7522000000013', 'Paleta de Caramelo', 'Paleta de caramelo con sabor a fresa.', 'Chucherías', 45, NULL),
(176, '7721520072100', 'Caramelos Duros', 'Caramelos duros surtidos.', 'Chucherías', 35, NULL),
(177, '7521011505201', 'Chocolate Blanco', 'Tableta de chocolate blanco.', 'Chocolates', 20, NULL),
(178, '7521025415207', 'Chicle de Menta', 'Paquete de chicles de menta.', 'Chucherías', 10, NULL),
(179, '7521710600035', 'Almendras Cubiertas de Chocolate', 'Almendras cubiertas de chocolate negro.', 'Chocolates', 15, NULL),
(180, '7521825306301', 'Regaliz', 'Tiras de regaliz sabor a fresa.', 'Chucherías', 30, NULL),
(181, '7521759600044', 'Bombones Rellenos', 'Bombones de chocolate rellenos de crema de avellana.', 'Chocolates', 20, NULL),
(182, '8410069001005', 'Barrita de Granola Orgánica', 'Barrita de granola orgánica con frutos secos.', 'Snacks Saludables', 25, NULL),
(183, '7501031311305', 'Chips de Kale', 'Chips crujientes de kale deshidratado.', 'Snacks Saludables', 40, NULL),
(184, '8412345678904', 'Frutas Secas Variadas', 'Mezcla de frutas secas: arándanos, pasas y nueces.', 'Snacks Saludables', 50, NULL),
(185, '7502000000015', 'Palitos de Zanahoria', 'Palitos de zanahoria frescos y crujientes.', 'Snacks Saludables', 10, NULL),
(186, '7791520072105', 'Hummus y Palitos de Apio', 'Hummus casero con palitos de apio frescos.', 'Snacks Saludables', 35, NULL),
(187, '7501011505205', 'Yogur Griego con Granola', 'Yogur griego natural con granola crujiente.', 'Snacks Saludables', 15, NULL),
(188, '7501025415205', 'Batido de Frutas', 'Batido de frutas naturales: plátano, fresas y kiwi.', 'Snacks Saludables', 25, NULL),
(189, '7501710600034', 'Edamame', 'Vainas de edamame cocidas y sazonadas.', 'Snacks Saludables', 20, NULL),
(190, '7501825306305', 'Muffin de Avena y Manzana', 'Muffin de avena y manzana sin azúcar añadido.', 'Snacks Saludables', 30, NULL),
(191, '7501759600045', 'Barrita de Frutas y Nueces', 'Barrita de frutas y nueces: arándanos, almendras y nueces.', 'Snacks Saludables', 45, NULL);

-- 
-- Dumping data for table ventas
--
INSERT INTO ventas VALUES
(1, 146, 44, 'Efectivo', 1.30, 2, '2024-06-09'),
(2, 154, 45, 'Tarjeta', 1.20, 3, '2024-06-09'),
(3, 169, 46, 'Efectivo', 1.30, 1, '2024-06-09'),
(4, 180, 47, 'Efectivo', 0.50, 4, '2024-06-09'),
(5, 190, 48, 'Efectivo', 2.00, 2, '2024-06-09'),
(6, 154, 45, 'Efectivo', 1.20, 7, '2024-06-09');

-- 
-- Dumping data for table piezas_reemplazadas
--
INSERT INTO piezas_reemplazadas VALUES
(1, 'Mecanismo dispensador'),
(1, 'Pantalla táctil'),
(2, 'Dispensador de productos'),
(2, 'Validador de billetes'),
(3, 'Cerradura de la puerta'),
(4, 'Cerradura de la puerta'),
(4, 'Dispensador de productos'),
(4, 'Mecanismo dispensador'),
(4, 'Validador de billetes');

-- 
-- Dumping data for table maquinas_productos
--
INSERT INTO maquinas_productos VALUES
(44, 144, 10),
(44, 145, 10),
(44, 146, 8),
(44, 147, 10),
(44, 148, 10),
(44, 149, 10),
(44, 150, 10),
(44, 151, 10),
(44, 152, 10),
(44, 153, 10),
(45, 154, 0),
(45, 155, 10),
(45, 156, 10),
(45, 157, 10),
(45, 158, 10),
(45, 159, 10),
(45, 160, 10),
(45, 161, 10),
(45, 162, 10),
(45, 163, 10),
(46, 164, 10),
(46, 165, 10),
(46, 166, 10),
(46, 167, 10),
(46, 168, 10),
(46, 169, 9),
(46, 170, 10),
(46, 171, 10),
(47, 172, 10),
(47, 173, 10),
(47, 174, 10),
(47, 175, 10),
(47, 176, 10),
(47, 177, 10),
(47, 178, 10),
(47, 179, 10),
(47, 180, 6),
(47, 181, 10),
(48, 182, 10),
(48, 183, 10),
(48, 184, 10),
(48, 185, 10),
(48, 186, 10),
(48, 187, 10),
(48, 188, 10),
(48, 189, 10),
(48, 190, 8),
(48, 191, 10);

-- 
-- Dumping data for table mantenimientos
--
INSERT INTO mantenimientos VALUES
(1, 44, 'Limpieza de dispensador de productos.', 1, '2024-06-08'),
(2, 45, 'Revisión de funcionamiento de pantalla táctil.', 1, '2024-12-06'),
(3, 46, 'Verificación de mecanismo de aceptación de billetes.', 0, '2025-01-18'),
(4, 47, 'Inspección de sistema de refrigeración.', 0, '2024-08-17'),
(5, 48, 'Revisión de conectividad de red.', 0, '2024-10-24');

-- 
-- Dumping data for table empresas_rutas
--
INSERT INTO empresas_rutas VALUES
(39, 40),
(40, 40),
(40, 41),
(43, 40),
(43, 41),
(44, 40),
(44, 41);

-- 
-- Dumping data for table contratos
--
INSERT INTO contratos VALUES
(17, 43, 44, 'Porcentaje', '10 % de las ventas para la empresa', '2024-06-03', '2025-04-23'),
(18, 39, 45, 'Porcentaje', '20% de las ventas para la empresa', '2024-06-06', '2024-10-31'),
(19, 44, 46, 'Porcentaje', '15% de las ventas para la empresa', '2024-06-05', '2025-03-13'),
(20, 40, 47, 'Porcentaje', '9% de las ventas para la empresa', '2024-05-27', '2025-07-08'),
(21, 43, 48, 'Porcentaje', '12% de las ventas para la empresa', '2024-05-29', '2025-03-12');

-- 
-- Dumping data for table compras
--
INSERT INTO compras VALUES
(129, 144, 'Proveedor A', 50, 10.00, '2024-06-08'),
(130, 145, 'Proveedor B', 30, 15.00, '2024-06-07'),
(131, 146, 'Proveedor C', 20, 12.00, '2024-06-06'),
(132, 147, 'Proveedor D', 25, 20.00, '2024-06-05'),
(133, 148, 'Proveedor E', 40, 8.00, '2024-06-04'),
(134, 149, 'Proveedor F', 30, 9.00, '2024-06-03'),
(135, 150, 'Proveedor G', 35, 7.00, '2024-06-02'),
(136, 151, 'Proveedor H', 50, 11.00, '2024-06-01'),
(137, 152, 'Proveedor I', 60, 6.00, '2024-05-31'),
(138, 153, 'Proveedor J', 20, 14.00, '2024-05-30'),
(139, 154, 'Proveedor K', 45, 18.00, '2024-05-29'),
(140, 155, 'Proveedor L', 25, 16.00, '2024-05-28'),
(141, 156, 'Proveedor M', 35, 17.00, '2024-05-27'),
(142, 157, 'Proveedor N', 30, 13.00, '2024-05-26'),
(143, 158, 'Proveedor O', 40, 19.00, '2024-05-25'),
(144, 159, 'Proveedor P', 55, 20.00, '2024-06-08'),
(145, 160, 'Proveedor Q', 45, 15.00, '2024-06-07'),
(146, 161, 'Proveedor R', 30, 10.00, '2024-06-06'),
(147, 162, 'Proveedor S', 20, 12.00, '2024-06-05'),
(148, 163, 'Proveedor T', 25, 20.00, '2024-06-04'),
(149, 164, 'Proveedor U', 40, 8.00, '2024-06-03'),
(150, 165, 'Proveedor V', 30, 9.00, '2024-06-02'),
(151, 166, 'Proveedor W', 35, 7.00, '2024-06-01'),
(152, 167, 'Proveedor X', 50, 11.00, '2024-05-31'),
(153, 168, 'Proveedor Y', 60, 6.00, '2024-05-30'),
(154, 169, 'Proveedor Z', 20, 14.00, '2024-05-29'),
(155, 170, 'Proveedor AA', 45, 18.00, '2024-05-28'),
(156, 171, 'Proveedor AB', 25, 16.00, '2024-05-27'),
(157, 172, 'Proveedor AC', 35, 17.00, '2024-05-26'),
(158, 173, 'Proveedor AD', 30, 13.00, '2024-05-25'),
(159, 174, 'Proveedor AE', 40, 19.00, '2024-06-08'),
(160, 175, 'Proveedor AF', 55, 20.00, '2024-06-07'),
(161, 176, 'Proveedor AG', 45, 15.00, '2024-06-06'),
(162, 177, 'Proveedor AH', 30, 10.00, '2024-06-05'),
(163, 178, 'Proveedor AI', 20, 12.00, '2024-06-04'),
(164, 179, 'Proveedor AJ', 25, 20.00, '2024-06-03'),
(165, 180, 'Proveedor AK', 40, 8.00, '2024-06-02'),
(166, 181, 'Proveedor AL', 30, 9.00, '2024-06-01'),
(167, 182, 'Proveedor AM', 35, 7.00, '2024-05-31'),
(168, 183, 'Proveedor AN', 50, 11.00, '2024-05-30'),
(169, 184, 'Proveedor AO', 60, 6.00, '2024-05-29'),
(170, 185, 'Proveedor AP', 20, 14.00, '2024-05-28'),
(171, 186, 'Proveedor AQ', 45, 18.00, '2024-05-27'),
(172, 187, 'Proveedor AR', 25, 16.00, '2024-05-26'),
(173, 188, 'Proveedor AS', 35, 17.00, '2024-05-25'),
(174, 189, 'Proveedor AT', 30, 13.00, '2024-06-08'),
(175, 190, 'Proveedor AU', 40, 19.00, '2024-06-07'),
(176, 191, 'Proveedor AV', 55, 20.00, '2024-06-06');

--
-- Set default database
--
USE vending_db;

DELIMITER $$

--
-- Create trigger `after_delete_maquinas_productos`
--
CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER IF NOT EXISTS after_delete_maquinas_productos
	AFTER DELETE
	ON maquinas_productos
	FOR EACH ROW
BEGIN
  IF @context IS NULL THEN
SET @context = 'asociar_maquina';
UPDATE productos
SET stock = stock + OLD.stock
WHERE id = OLD.idProductos;
SET @context = NULL;
  END IF;
END
$$

--
-- Create trigger `after_insert_maquinas_productos`
--
CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER IF NOT EXISTS after_insert_maquinas_productos
	AFTER INSERT
	ON maquinas_productos
	FOR EACH ROW
BEGIN
  IF @context IS NULL THEN
SET @context = 'asociar_maquina';
UPDATE productos
SET stock = stock - NEW.stock
WHERE id = NEW.idProductos;
SET @context = NULL;
  END IF;
END
$$

--
-- Create trigger `before_update_maquinas_productos`
--
CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER IF NOT EXISTS before_update_maquinas_productos
	BEFORE UPDATE
	ON maquinas_productos
	FOR EACH ROW
BEGIN
  IF @context IS NULL THEN
SET @context = 'actualizar_maquina';
SET @stock_diff = NEW.stock - OLD.stock;
UPDATE productos
SET stock = stock - @stock_diff
WHERE id = NEW.idProductos;
SET @context = NULL;
  END IF;
END
$$

--
-- Create trigger `after_delete_ventas`
--
CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER IF NOT EXISTS after_delete_ventas
	AFTER DELETE
	ON ventas
	FOR EACH ROW
BEGIN
  IF @context IS NULL THEN
SET @context = 'venta';
UPDATE maquinas_productos
SET stock = stock + OLD.cantidad
WHERE idMaquinas = OLD.idMaquinas
AND idProductos = OLD.idProductos;
SET @context = NULL;
  END IF;
END
$$

--
-- Create trigger `after_insert_ventas`
--
CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER IF NOT EXISTS after_insert_ventas
	AFTER INSERT
	ON ventas
	FOR EACH ROW
BEGIN
  DECLARE available_stock INT;

  IF @context IS NULL THEN
SET available_stock = (SELECT
    stock
  FROM maquinas_productos
  WHERE idMaquinas = NEW.idMaquinas
  AND idProductos = NEW.idProductos);
    
    IF available_stock >= NEW.cantidad THEN
SET @context = 'venta';
UPDATE maquinas_productos
SET stock = stock - NEW.cantidad
WHERE idMaquinas = NEW.idMaquinas
AND idProductos = NEW.idProductos;
SET @context = NULL;
    ELSE
      SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Stock insuficiente para realizar la venta';
    END IF;
  END IF;
END
$$

--
-- Create trigger `before_update_ventas`
--
CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER IF NOT EXISTS before_update_ventas
	BEFORE UPDATE
	ON ventas
	FOR EACH ROW
BEGIN
  DECLARE old_stock INT;
  DECLARE new_stock INT;

  IF @context IS NULL THEN
SET old_stock = (SELECT
    stock
  FROM maquinas_productos
  WHERE idMaquinas = OLD.idMaquinas
  AND idProductos = OLD.idProductos);
SET new_stock = (SELECT
    stock
  FROM maquinas_productos
  WHERE idMaquinas = NEW.idMaquinas
  AND idProductos = NEW.idProductos);

    IF old_stock IS NOT NULL THEN
SET @context = 'venta';
UPDATE maquinas_productos
SET stock = stock + OLD.cantidad
WHERE idMaquinas = OLD.idMaquinas
AND idProductos = OLD.idProductos;
SET @context = NULL;
    END IF;

    IF new_stock IS NOT NULL AND new_stock >= NEW.cantidad THEN
SET @context = 'venta';
UPDATE maquinas_productos
SET stock = stock - NEW.cantidad
WHERE idMaquinas = NEW.idMaquinas
AND idProductos = NEW.idProductos;
SET @context = NULL;
    ELSE
      SIGNAL SQLSTATE '45000'
SET MESSAGE_TEXT = 'Stock insuficiente para actualizar la venta';
    END IF;
  END IF;
END
$$

--
-- Create trigger `after_delete_compras`
--
CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER IF NOT EXISTS after_delete_compras
	AFTER DELETE
	ON compras
	FOR EACH ROW
BEGIN
  IF @context IS NULL THEN
SET @context = 'compra';
UPDATE productos
SET stock = stock - OLD.cantidad
WHERE id = OLD.idProductos;
SET @context = NULL;
  END IF;
END
$$

--
-- Create trigger `after_insert_compras`
--
CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER IF NOT EXISTS after_insert_compras
	AFTER INSERT
	ON compras
	FOR EACH ROW
BEGIN
  IF @context IS NULL THEN
SET @context = 'compra';
UPDATE productos
SET stock = stock + NEW.cantidad
WHERE id = NEW.idProductos;
SET @context = NULL;
  END IF;
END
$$

--
-- Create trigger `after_update_compras`
--
CREATE 
	DEFINER = 'root'@'localhost'
TRIGGER IF NOT EXISTS after_update_compras
	AFTER UPDATE
	ON compras
	FOR EACH ROW
BEGIN
  IF @context IS NULL THEN
SET @context = 'compra';
    IF NEW.cantidad <> OLD.cantidad THEN
UPDATE productos
SET stock = stock + (NEW.cantidad - OLD.cantidad)
WHERE id = NEW.idProductos;
    END IF;
SET @context = NULL;
  END IF;
END
$$

DELIMITER ;

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;