<?php

namespace app\controllers;

use app\models\MaquinasProductos;
use app\models\Productos;
use app\models\search\MaquinasProductosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Yii;
/**
 * MaquinasProductosController implements the CRUD actions for MaquinasProductos model.
 */
class MaquinasProductosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all MaquinasProductos models.
     *
     * @return string
     */
    // En el controlador MaquinasProductosController.php

    public function actionIndex($id = null)
    {
        $searchModel = new MaquinasProductosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($id !== null) {
            $dataProvider->query->andFilterWhere(['idMaquinas' => $id]);
            $model = MaquinasProductos::find()->where(['idMaquinas' => $id])->one();
        } else {
            $model = null; // No hay un id específico
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model,
        ]);
    }



    /**
     * Displays a single MaquinasProductos model.
     * @param int $idMaquinas Id Maquinas
     * @param int $idProductos Id Productos
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($idMaquinas, $idProductos)
    {
        return $this->render('view', [
            'model' => $this->findModel($idMaquinas, $idProductos),
        ]);
    }

    /**
     * Creates a new MaquinasProductos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
public function actionCreate()
{
    $model = new MaquinasProductos();
    
    if ($model->load(Yii::$app->request->post())) {
        $productos = Yii::$app->request->post('MaquinasProductos')['idProductos'];
        $idMaquinas = $model->idMaquinas;
        $errores = false;
        $erroresProductos = [];
        
        // Iniciar una transacción
        $transaction = Yii::$app->db->beginTransaction();
        
        try {
            foreach ($productos as $producto) {
                $maquinaProducto = new MaquinasProductos();
                $maquinaProducto->idMaquinas = $idMaquinas;
                $maquinaProducto->idProductos = $producto;
                $maquinaProducto->stock = 0; // Valor predeterminado

                if (!$maquinaProducto->save()) {
                    // Obtener el nombre del producto desde el modelo Producto
                    $nombreProducto = Productos::findOne($producto)->nombre;
                    $errores = true;
                    $erroresProductos[] = $nombreProducto;
                    // No hacemos break aquí para recopilar todos los productos con errores
                }
            }

            if ($errores) {
                $transaction->rollBack();
                $model->addError('idProductos', 'Los siguientes productos ya están asociados a esta máquina: ' . implode(', ', $erroresProductos));
            } else {
                $transaction->commit();
                return $this->redirect(['maquinas/index', 'id' => $idMaquinas]);
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            throw $e;
        } catch (\Throwable $e) {
            $transaction->rollBack();
            throw $e;
        }
    }

    return $this->render('create', [
        'model' => $model,
    ]);
}






    /**
     * Updates an existing MaquinasProductos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $idMaquinas Id Maquinas
     * @param int $idProductos Id Productos
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($idMaquinas, $idProductos)
    {
        $model = $this->findModel($idMaquinas, $idProductos);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'idMaquinas' => $model->idMaquinas, 'idProductos' => $model->idProductos]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing MaquinasProductos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $idMaquinas Id Maquinas
     * @param int $idProductos Id Productos
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($idMaquinas, $idProductos)
    {
        $this->findModel($idMaquinas, $idProductos)->delete();

        return $this->redirect(['maquinas-productos/index', 'id' => $idMaquinas]);
    }

    /**
     * Finds the MaquinasProductos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $idMaquinas Id Maquinas
     * @param int $idProductos Id Productos
     * @return MaquinasProductos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($idMaquinas, $idProductos)
    {
        if (($model = MaquinasProductos::findOne(['idMaquinas' => $idMaquinas, 'idProductos' => $idProductos])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
