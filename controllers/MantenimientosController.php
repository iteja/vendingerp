<?php

namespace app\controllers;

use Yii;
use app\models\Mantenimientos;
use app\models\search\MantenimientosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MantenimientosController implements the CRUD actions for Mantenimientos model.
 */
class MantenimientosController extends Controller
{
    public function actionUpdateEstado()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON; // Asegúrate de que la respuesta es JSON

        if (Yii::$app->request->isPost) {
            $id = Yii::$app->request->post('id');
            $estado = Yii::$app->request->post('estado');

            $model = Mantenimientos::findOne($id);
            if ($model) {
                $model->estado = (int)$estado; // Convertir el estado a entero
                if ($model->save()) {
                    return ['success' => true];
                } else {
                    return ['success' => false, 'error' => $model->getErrors()];
                }
            }
        }

        return ['success' => false, 'error' => 'Invalid request'];
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Mantenimientos models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new MantenimientosSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Mantenimientos model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Mantenimientos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Mantenimientos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Mantenimientos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
public function actionUpdate($id)
{
    $model = $this->findModel($id);

    if ($this->request->isPost) {
        $estadoAnterior = $model->estado; // Guarda el estado anterior

        if ($model->load($this->request->post())) {
            // Si el estado se ha cambiado manualmente, respetar ese cambio
            if ($model->isAttributeChanged('estado')) {
                // Evita que beforeSave modifique el estado si ya fue cambiado manualmente
                $model->estado = (int)$model->estado;
            } else {
                // Mantén el estado anterior si no fue cambiado
                $model->estado = $estadoAnterior;
            }

            if ($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
    }

    return $this->render('update', [
        'model' => $model,
    ]);
}


    /**
     * Deletes an existing Mantenimientos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Mantenimientos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Mantenimientos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Mantenimientos::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
