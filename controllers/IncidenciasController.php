<?php

namespace app\controllers;

use app\models\Incidencias;
use app\models\PiezasReemplazadas;
use app\models\search\IncidenciasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * IncidenciasController implements the CRUD actions for Incidencias model.
 */
class IncidenciasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Incidencias models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new IncidenciasSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Incidencias model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Incidencias model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
{
    $model = new Incidencias();

    if ($this->request->isPost) {
        if ($model->load($this->request->post()) && $model->save()) {
            $piezasReemplazadas = Yii::$app->request->post('PiezasReemplazadas');
            foreach ($piezasReemplazadas as $pieza) {
                if ($pieza != '') {
                    $piezaModel = new PiezasReemplazadas();
                    $piezaModel->idIncidencias = $model->id;
                    $piezaModel->piezas_reemplazadas = $pieza;
                    $piezaModel->save();
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }
    } else {
        $model->loadDefaultValues();
    }

    return $this->render('create', [
        'model' => $model,
        'piezaValues' => [],
    ]);
}


    /**
     * Updates an existing Incidencias model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
{
    $model = $this->findModel($id);
    $piezasReemplazadas = $model->getPiezasReemplazadas()->all();
    $piezaValues = array_map(function ($pieza) {
        return $pieza->piezas_reemplazadas;
    }, $piezasReemplazadas);

    if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
        PiezasReemplazadas::deleteAll(['idIncidencias' => $model->id]);
        $piezasReemplazadas = Yii::$app->request->post('PiezasReemplazadas');
        foreach ($piezasReemplazadas as $pieza) {
            if ($pieza != '') {
                $piezaModel = new PiezasReemplazadas();
                $piezaModel->idIncidencias = $model->id;
                $piezaModel->piezas_reemplazadas = $pieza;
                $piezaModel->save();
            }
        }
        return $this->redirect(['view', 'id' => $model->id]);
    }

    return $this->render('update', [
        'model' => $model,
        'piezaValues' => $piezaValues,
    ]);
}


    /**
     * Deletes an existing Incidencias model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Incidencias model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Incidencias the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Incidencias::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
