<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Ventas;
use yii\db\Query;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
public function actionIndex()
{
    // Consulta para obtener las máquinas con más incidencias
    $incidenciasData = (new Query())
        ->select(['m.nombre', 'COUNT(i.id) AS numero_incidencias'])
        ->from('maquinas m')
        ->join('JOIN', 'incidencias i', 'm.id = i.idMaquinas')
        ->groupBy('m.id, m.nombre')
        ->orderBy(['numero_incidencias' => SORT_DESC])
        ->limit(3)
        ->all();

    // Consulta para obtener las piezas más reemplazadas
    $reemplazadasData = (new Query())
        ->select(['piezas_reemplazadas', 'COUNT(*) AS veces_reemplazadas'])
        ->from('piezas_reemplazadas')
        ->groupBy('piezas_reemplazadas')
        ->orderBy(['veces_reemplazadas' => SORT_DESC])
        
        ->all();

    // Consulta para obtener las máquinas con más ventas
    $ventasData = (new Query())
        ->select(['m.nombre', 'COUNT(v.id) AS numero_ventas'])
        ->from('maquinas m')
        ->join('JOIN', 'ventas v', 'm.id = v.idMaquinas')
        ->groupBy('m.id, m.nombre')
        ->orderBy(['numero_ventas' => SORT_DESC])
        ->limit(3)
        ->all();

    // Consulta para obtener los productos más vendidos
    $productosData = (new Query())
        ->select(['p.nombre', 'SUM(v.precio) AS total_ventas', 'COUNT(v.id) AS numero_ventas'])
        ->from('productos p')
        ->join('JOIN', 'ventas v', 'p.id = v.idProductos')
        ->groupBy('p.id, p.nombre')
        ->orderBy(['numero_ventas' => SORT_DESC])
        
        ->all();

    // Consulta para obtener las máquinas con menos ventas
    $maquinasMenosVentasData = (new Query())
        ->select(['m.nombre', 'COUNT(v.id) AS numero_ventas'])
        ->from('maquinas m')
        ->join('JOIN', 'ventas v', 'm.id = v.idMaquinas')
        ->groupBy('m.id, m.nombre')
        ->orderBy(['numero_ventas' => SORT_ASC])
        ->limit(3)
        ->all();

    // Consulta para obtener los productos con menos ventas
    $productosMenosVentasData = (new Query())
        ->select(['p.nombre', 'SUM(v.precio) AS total_ventas', 'COUNT(v.id) AS numero_ventas'])
        ->from('productos p')
        ->join('JOIN', 'ventas v', 'p.id = v.idProductos')
        ->groupBy('p.id, p.nombre')
        ->orderBy(['numero_ventas' => SORT_ASC])
        
        ->all();

    return $this->render('index', [
        'incidenciasData' => $incidenciasData,
        'reemplazadasData' => $reemplazadasData,
        'ventasData' => $ventasData,
        'productosData' => $productosData,
        'maquinasMenosVentasData' => $maquinasMenosVentasData,
        'productosMenosVentasData' => $productosMenosVentasData,
    ]);
}



    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionObtenerProductos($maquinaId)
{
    $productos = \app\models\Productos::find()
        ->where(['idMaquinas' => $maquinaId])
        ->all();

    $options = '<option value="">Seleccione un Producto</option>';
    foreach ($productos as $producto) {
        $options .= '<option value="' . $producto->id . '">' . $producto->nombre . '</option>';
    }

    Yii::$app->response->format = Response::FORMAT_JSON;
    return ['options' => $options];
}

    
    
}
