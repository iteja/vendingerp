<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $id
 * @property string $codigo_barras
 * @property string $nombre
 * @property string|null $descripcion
 * @property string|null $categoria
 * @property int $stock
 *
 * @property Compras[] $compras
 * @property Maquinas[] $idMaquinas
 * @property MaquinasProductos[] $maquinasProductos
 * @property Ventas[] $ventas
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required', 'message' => 'Debe de introducir un nombre'],
            [['codigo_barras'], 'required', 'message' => 'Debe de introducir un código de barras'],
            
            [['descripcion'], 'string', 'max' => 255],
            [['stock'], 'integer'],
            [['nombre', 'categoria'], 'string', 'max' => 60],
            [['codigo_barras'], 'unique'],
            [['codigo_barras'], 'validateBarcode'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_barras' => 'Código de Barras',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripción',
            'categoria' => 'Categoria',
            'stock' => 'Stock',
        ];
    }

    /**
     * Gets query for [[Compras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompras()
    {
        return $this->hasMany(Compras::class, ['idProductos' => 'id']);
    }

    /**
     * Gets query for [[IdMaquinas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMaquinas()
    {
        return $this->hasMany(Maquinas::class, ['id' => 'idMaquinas'])->viaTable('maquinas_productos', ['idProductos' => 'id']);
    }

    /**
     * Gets query for [[MaquinasProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaquinasProductos()
    {
        return $this->hasMany(MaquinasProductos::class, ['idProductos' => 'id']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::class, ['idProductos' => 'id']);
    }
    
    public function getUltimaCompra()
{
    return $this->hasOne(Compras::class, ['idProductos' => 'id'])->orderBy(['fecha' => SORT_DESC]);
}

public function getUltimaVenta()
{
    return $this->hasOne(Ventas::class, ['idProductos' => 'id'])->orderBy(['fecha' => SORT_DESC]);
}

public function validateBarcode($attribute, $params)
    {
        $value = $this->$attribute;
        // Check if it is 13 digits long
        if (!preg_match('/^\d{13}$/', $value)) {
            $this->addError($attribute, 'El código de barras debe tener exactamente 13 dígitos.');
            return;
        }

        // Validate EAN-13 checksum
        $checksum = 0;
        for ($i = 0; $i < 12; $i++) {
            $digit = (int) $value[$i];
            if ($i % 2 == 0) {
                $checksum += $digit;
            } else {
                $checksum += $digit * 3;
            }
        }
        $checksum = (10 - ($checksum % 10)) % 10;
        if ($checksum != (int) $value[12]) {
            $this->addError($attribute, 'El código de barras no es válido.');
        }
    }
    

    /**
     * Gets query for [[MaquinasProductos]].
     *
     * @return \yii\db\ActiveQuery
     */


    /**
     * Gets query for [[Maquinas]] through [[MaquinasProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaquinas()
    {
        return $this->hasMany(Maquinas::class, ['id' => 'idMaquinas'])->via('maquinasProductos');
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */

 

}
