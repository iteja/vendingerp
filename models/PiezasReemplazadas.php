<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "piezas_reemplazadas".
 *
 * @property int $idIncidencias
 * @property string $piezas_reemplazadas
 *
 * @property Incidencias $idIncidencias0
 */
class PiezasReemplazadas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'piezas_reemplazadas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idIncidencias', 'piezas_reemplazadas'], 'required'],
            [['idIncidencias'], 'integer'],
            [['piezas_reemplazadas'], 'string', 'max' => 255],
            [['idIncidencias', 'piezas_reemplazadas'], 'unique', 'targetAttribute' => ['idIncidencias', 'piezas_reemplazadas']],
            [['idIncidencias'], 'exist', 'skipOnError' => true, 'targetClass' => Incidencias::class, 'targetAttribute' => ['idIncidencias' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idIncidencias' => 'Id Incidencias',
            'piezas_reemplazadas' => 'Piezas Reemplazadas',
        ];
    }

    /**
     * Gets query for [[IdIncidencias0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdIncidencias0()
    {
        return $this->hasOne(Incidencias::class, ['id' => 'idIncidencias']);
    }
}
