<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "incidencias".
 *
 * @property int $id
 * @property int $idMaquinas
 * @property string $descripcion
 * @property string $fecha
 *
 * @property Maquinas $idMaquinas0
 * @property PiezasReemplazadas[] $piezasReemplazadas
 */
class Incidencias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'incidencias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idMaquinas', 'fecha'], 'required'],
            
            [['idMaquinas'], 'integer'],
            [['descripcion'], 'string', 'max' => 255],
            [['fecha'], 'safe'],
            [['fecha'], 'compare', 'compareValue' => date('Y-m-d'), 'operator' => '<=', 'type' => 'date', 'message' => 'La fecha no puede ser posterior a hoy.'],
            [['idMaquinas'], 'exist', 'skipOnError' => true, 'targetClass' => Maquinas::class, 'targetAttribute' => ['idMaquinas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idMaquinas' => 'Máquina',
            'descripcion' => 'Descripción',
            'fecha' => 'Fecha',
        ];
    }

    /**
     * Gets query for [[IdMaquinas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMaquinas0()
    {
        return $this->hasOne(Maquinas::class, ['id' => 'idMaquinas']);
    }

    /**
     * Gets query for [[PiezasReemplazadas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPiezasReemplazadas()
    {
        return $this->hasMany(PiezasReemplazadas::class, ['idIncidencias' => 'id']);
    }

    /**
     * Obtiene una lista de piezas reemplazadas en formato de texto
     *
     * @return string
     */
    public function getPiezasReemplazadasList()
    {
        $piezas = $this->piezasReemplazadas;
        $piezasList = array_map(function ($pieza) {
            return $pieza->piezas_reemplazadas;
        }, $piezas);
        return implode(', ', $piezasList);
    }
}
