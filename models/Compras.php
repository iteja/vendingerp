<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compras".
 *
 * @property int $id
 * @property int|null $idProductos
 * @property string|null $proveedor
 * @property int|null $cantidad
 * @property float|null $precio
 * @property string|null $fecha
 *
 * @property Productos $idProductos0
 */
class Compras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idProductos'], 'required', 'message' => 'Debe de seleccionar un producto.'],
            [['cantidad'], 'required', 'message' => 'Debe de introducir una cantidad.'],
            [['precio'], 'required', 'message' => 'Debe de introducir un precio.'],
            [['fecha'], 'required', 'message' => 'Debe de introducir una fecha'],
            
            
            [['proveedor'], 'string', 'max' => 60],
            [['cantidad'], 'integer', 'max' => 10000000],
            [['cantidad'], 'integer', 'min' => 1],

            [['precio'], 'validarPrecio'],
            [['fecha'], 'safe'],
                    [['fecha'], function ($attribute, $params, $validator) {
                if (strtotime($this->$attribute) > time()) {
                    $this->addError($attribute, 'La fecha no puede ser posterior al día de hoy.');
                }
            }],
            [['idProductos'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['idProductos' => 'id']],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idProductos' => 'Producto',
            'proveedor' => 'Proveedor',
            'cantidad' => 'Cantidad',
            'precio' => 'Precio total',
            'fecha' => 'Fecha de compra',
        ];
    }

    /**
     * Gets query for [[IdProductos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProductos0()
    {
        return $this->hasOne(Productos::class, ['id' => 'idProductos']);
    }
    
    // En el modelo Compras.php

    public function getProducto()
    {
        return $this->hasOne(Productos::class, ['id' => 'idProductos']);
    }
    

    
   public function validarPrecio($attribute, $params)
    {
        if (!preg_match('/^\d+(\.\d{1,2})?|\d+(,\d{1,2})?$/', $this->$attribute)) {
            $this->addError($attribute, 'El precio debe ser un número con hasta dos decimales, usando punto o coma como separador.');
        } elseif ($this->$attribute < 0.01) {
            $this->addError($attribute, 'El precio debe ser mayor o igual a 0,01.');
        } elseif ($this->$attribute > 10000000) {
            $this->addError($attribute, 'El precio no debe ser mayor a 10000000.');
        }
    }
    public function getFormattedCantidad()
    {
        return number_format($this->cantidad, 0, '', '.');
    }
    
}
