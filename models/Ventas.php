<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ventas".
 *
 * @property int $id
 * @property int $idProductos
 * @property int $idMaquinas
 * @property string $metodo_de_pago
 * @property float $precio
 * @property string $fecha
 * @property int $cantidad
 *
 * @property Maquinas $idMaquinas0
 * @property Productos $idProductos0
 */
class Ventas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ventas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idProductos', 'idMaquinas', 'metodo_de_pago', 'precio', 'fecha', 'cantidad'], 'required'],
            [['idProductos', 'idMaquinas', 'cantidad'], 'integer'],
            [['precio'], 'validarPrecio'],
            [['fecha'], 'safe'],
            [['cantidad'], 'integer', 'max' => 10000000],
            [['cantidad'], 'integer', 'min' => 1],
            [['metodo_de_pago'], 'string', 'max' => 255],
            [['idProductos'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['idProductos' => 'id']],
            [['idMaquinas'], 'exist', 'skipOnError' => true, 'targetClass' => Maquinas::class, 'targetAttribute' => ['idMaquinas' => 'id']],
            [['cantidad'], 'validarStock'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idProductos' => 'Producto',
            'idMaquinas' => 'Máquina',
            'metodo_de_pago' => 'Método de Pago',
            'precio' => 'Precio',
            'fecha' => 'Fecha',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * Gets query for [[IdMaquinas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMaquinas0()
    {
        return $this->hasOne(Maquinas::class, ['id' => 'idMaquinas']);
    }

    /**
     * Gets query for [[IdProductos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProductos0()
    {
        return $this->hasOne(Productos::class, ['id' => 'idProductos']);
    }

    public function validarPrecio($attribute, $params)
    {
        if (!preg_match('/^\d+(\.\d{1,2})?|\d+(,\d{1,2})?$/', $this->$attribute)) {
            $this->addError($attribute, 'El precio debe ser un número con hasta dos decimales, usando punto o coma como separador.');
        } elseif ($this->$attribute < 0.01) {
            $this->addError($attribute, 'El precio debe ser mayor o igual a 0,01.');
        } elseif ($this->$attribute > 10000000) {
            $this->addError($attribute, 'El precio no debe ser mayor a 10000000.');
        }
    }

public function validarStock($attribute, $params)
{
    $stock = MaquinasProductos::find()
        ->where(['idMaquinas' => $this->idMaquinas, 'idProductos' => $this->idProductos])
        ->one();

    if ($stock === null) {
        // El producto no está disponible en esta máquina.
        $this->addError('idProductos', 'El producto no está disponible en esta máquina.');
    } elseif ($stock->stock < $this->cantidad) {
        // No se puede realizar la venta porque la cantidad es mayor al stock disponible.
        $this->addError($attribute, 'No se puede realizar la venta porque la cantidad es mayor al stock disponible.');
    }
}

}
