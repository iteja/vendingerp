<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Maquinas;

/**
 * MaquinasSearch represents the model behind the search form of `app\models\Maquinas`.
 */
class MaquinasSearch extends Maquinas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'idEmpresas', 'estado'], 'integer'],
            [['numero_de_serie', 'nombre', 'tipo_de_maquina', 'fecha_de_ultima_instalacion'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Maquinas::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'idEmpresas' => $this->idEmpresas,
            'estado' => $this->estado,
            'fecha_de_ultima_instalacion' => $this->fecha_de_ultima_instalacion,
        ]);

        $query->andFilterWhere(['like', 'numero_de_serie', $this->numero_de_serie])
            ->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'tipo_de_maquina', $this->tipo_de_maquina]);

        return $dataProvider;
    }
}
