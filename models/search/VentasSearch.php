<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ventas;

class VentasSearch extends Ventas
{
    public $nombreProducto;
    public $codigoBarrasProducto;
    public $nombreMaquina;
    public $numeroSerieMaquina;

    public function rules()
    {
        return [
            [['id', 'idProductos', 'idMaquinas'], 'integer'],
            [['metodo_de_pago', 'fecha', 'nombreProducto', 'codigoBarrasProducto', 'nombreMaquina', 'numeroSerieMaquina'], 'safe'],
            [['precio'], 'number'],
        ];
    }

    public function search($params)
    {
        $query = Ventas::find()
            ->joinWith(['idProductos0', 'idMaquinas0']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'idProductos' => $this->idProductos,
            'idMaquinas' => $this->idMaquinas,
            'precio' => $this->precio,
            'fecha' => $this->fecha,
        ]);

        $query->andFilterWhere(['like', 'metodo_de_pago', $this->metodo_de_pago])
            ->andFilterWhere(['like', 'productos.nombre', $this->nombreProducto])
            ->andFilterWhere(['like', 'productos.codigo_barras', $this->codigoBarrasProducto])
            ->andFilterWhere(['like', 'maquinas.nombre', $this->nombreMaquina])
            ->andFilterWhere(['like', 'maquinas.numero_de_serie', $this->numeroSerieMaquina]);

        return $dataProvider;
    }
}
