<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Compras;

class ComprasSearch extends Compras
{
    public $codigo_barras; // Para el código de barras del producto

    public function rules()
    {
        return [
            [['id', 'idProductos', 'cantidad'], 'integer'],
            [['proveedor', 'fecha', 'codigo_barras'], 'safe'],
            [['precio'], 'number'],
        ];
    }

    public function search($params)
    {
        $query = Compras::find()->joinWith('producto'); // Relación con productos

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false, // Desactiva la paginación para buscar en todas las páginas
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'idProductos' => $this->idProductos,
            'cantidad' => $this->cantidad,
            'precio' => $this->precio,
            'fecha' => $this->fecha,
        ]);

        $query->andFilterWhere(['like', 'proveedor', $this->proveedor])
              ->andFilterWhere(['like', 'productos.codigo_barras', $this->codigo_barras]); // Asegúrate de que el alias sea correcto

        return $dataProvider;
    }
}
