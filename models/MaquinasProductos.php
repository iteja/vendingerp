<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maquinas_productos".
 *
 * @property int $idMaquinas
 * @property int $idProductos
 * @property int $stock
 *
 * @property Maquinas $idMaquinas0
 * @property Productos $idProductos0
 */
class MaquinasProductos extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'maquinas_productos';
    }

    public function rules()
    {
        return [
            [['idMaquinas', 'idProductos'], 'required'],
            [['idMaquinas', 'idProductos', 'stock'], 'integer'],
            [['stock'], 'default', 'value' => 0],
            [['idMaquinas', 'idProductos'], 'unique', 'targetAttribute' => ['idMaquinas', 'idProductos'], 'message' => 'Este producto ya está asociado a esta máquina.'],
            [['idMaquinas'], 'exist', 'skipOnError' => true, 'targetClass' => Maquinas::class, 'targetAttribute' => ['idMaquinas' => 'id']],
            [['idProductos'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['idProductos' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'idMaquinas' => 'Máquina',
            'idProductos' => 'Productos',
            'stock' => 'Stock',
        ];
    }

    public function getIdMaquinas0()
    {
        return $this->hasOne(Maquinas::class, ['id' => 'idMaquinas']);
    }

    public function getIdProductos0()
    {
        return $this->hasOne(Productos::class, ['id' => 'idProductos']);
    }

    public function getProducto()
    {
        return $this->hasOne(Productos::class, ['id' => 'idProductos']);
    }
}
