<?php

namespace app\models;

use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "contratos".
 *
 * @property int $id
 * @property int $idEmpresas
 * @property int $idMaquinas
 * @property string $tipo
 * @property string|null $descripcion
 * @property string $fecha_de_inicio
 * @property string $fecha_fin
 *
 * @property Empresas $idEmpresas0
 * @property Maquinas $idMaquinas0
 */
class Contratos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contratos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEmpresas'], 'required', 'message' => 'Debe de seleccionar una empresa.'],
            [['idMaquinas'], 'required', 'message' => 'Debe de seleccionar una maquina.'],
            [['fecha_fin'], 'required', 'message' => 'Debe de introducir una fecha fin'],
            [['fecha_de_inicio'], 'required', 'message' => 'Debe de introducir una fecha de inicio'],
            [['tipo'], 'required', 'message' => 'Debe de introducir el tipo de contrato.'],

            [['idEmpresas', 'idMaquinas'], 'integer'],
            [['descripcion'], 'string', 'max' => 255],
            [['fecha_de_inicio', 'fecha_fin'], 'safe'],
            [['tipo'], 'string', 'max' => 60],
            [['idEmpresas'], 'exist', 'skipOnError' => true, 'targetClass' => Empresas::class, 'targetAttribute' => ['idEmpresas' => 'id']],
            [['idMaquinas'], 'exist', 'skipOnError' => true, 'targetClass' => Maquinas::class, 'targetAttribute' => ['idMaquinas' => 'id']],
            [['fecha_de_inicio'], 'compare', 'compareAttribute' => 'fecha_fin', 'operator' => '<=', 'type' => 'date', 'message' => 'La fecha de inicio debe ser anterior o igual a la fecha de fin.', 
                'when' => function($model) {
                    return !empty($model->fecha_fin);
                },
                'whenClient' => "function (attribute, value) {
                    return $('#" . Html::getInputId($this, 'fecha_fin') . "').val() != '';
                }"
            ],
            [['fecha_fin'], 'compare', 'compareAttribute' => 'fecha_de_inicio', 'operator' => '>=', 'type' => 'date', 'message' => 'La fecha de fin debe ser posterior o igual a la fecha de inicio.'],
            ['idMaquinas', 'validateDates'],
        ];
    }

    /**
     * Custom validation method to prevent overlapping contracts for the same machine.
     */
    public function validateDates($attribute, $params, $validator)
    {
        $exists = self::find()
            ->where(['idMaquinas' => $this->idMaquinas])
            ->andWhere(['not', ['id' => $this->id]])
            ->andWhere(['<=', 'fecha_de_inicio', $this->fecha_fin])
            ->andWhere(['>=', 'fecha_fin', $this->fecha_de_inicio])
            ->exists();

        if ($exists) {
            $this->addError($attribute, 'Esta máquina ya tiene un contrato en las fechas seleccionadas.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idEmpresas' => 'Empresa',
            'idMaquinas' => 'Máquina',
            'tipo' => 'Tipo',
            'descripcion' => 'Descripción',
            'fecha_de_inicio' => 'Fecha de Inicio',
            'fecha_fin' => 'Fecha de Fin',
        ];
    }

    /**
     * Gets query for [[IdEmpresas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresas0()
    {
        return $this->hasOne(Empresas::class, ['id' => 'idEmpresas']);
    }

    /**
     * Gets query for [[IdMaquinas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMaquinas0()
    {
        return $this->hasOne(Maquinas::class, ['id' => 'idMaquinas']);
    }
    
    public function getDuracion()
    {
        $inicio = new \DateTime($this->fecha_de_inicio);
        $fin = new \DateTime($this->fecha_fin);
        $intervalo = $inicio->diff($fin);

        // Formatear la duración
        $duracion = '';
        if ($intervalo->y > 0) {
            $duracion .= $intervalo->y . ' año/s';
        }
        if ($intervalo->m > 0) {
            $duracion .= ($duracion !== '' ? ', ' : '') . $intervalo->m . ' mes/es';
        }
        if ($intervalo->d > 0) {
            $duracion .= ($duracion !== '' ? ' y ' : '') . $intervalo->d . ' día/s';
        }

        return $duracion;
    }
}
