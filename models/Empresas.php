<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empresas".
 *
 * @property int $id
 * @property string $nombre
 * @property string|null $industria
 * @property string|null $ciudad
 * @property string|null $coordenadas
 * @property string|null $direccion
 * @property string|null $representante
 * @property string|null $telefono
 * @property string|null $correo
 *
 * @property Contratos[] $contratos
 * @property EmpresasRutas[] $empresasRutas
 * @property Rutas[] $idRutas
 * @property Maquinas[] $maquinas
 */
class Empresas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required', 'message' => 'Debe de introducir un nombre.'],
            [['coordenadas'], 'required', 'message' => 'Debe de introducir unas coordenadas.'],
            [['ciudad'], 'required', 'message' => 'Debe de introducir una ciudad.'],
            [['direccion'], 'required', 'message' => 'Debe de introducir una direccion.'],
            [['representante'], 'required', 'message' => 'Debe de introducir un representante.'],
            //[['telefono'], 'required', 'message' => 'Debe de introducir un telefono.'],

            [['ciudad', 'direccion'], 'string', 'max' => 100],
            [['nombre', 'industria', 'representante'], 'string', 'max' => 60],
            [['telefono'], 'string', 'max' => 9],
            [['telefono'], 'match', 'pattern' => '/^\d{9}$/', 'message' => 'Debe de introducir un teléfono válido de 9 dígitos.'],  // Validación para un teléfono de 9 dígitos
            [['correo'], 'string', 'max' => 255],

            //[['correo'], 'unique', 'message' => 'Este correo ya está en uso.'],
            [['correo'], 'email', 'message' => 'Debe de introducir un correo válido.'],

            [['coordenadas'], 'match', 'pattern' => '/^[-+]?\d{1,2}\.\d{5},\s*[-+]?\d{1,3}\.\d{5}$/', 'message' => 'Debe de introducir coordenadas válidas en el formato "latitud, longitud".'],

        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'industria' => 'Industria',
            'ciudad' => 'Ciudad',
            'coordenadas' => 'Coordenadas',
            'direccion' => 'Dirección',
            'representante' => 'Representante',
            'telefono' => 'Teléfono',
            'correo' => 'Correo',
        ];
    }

    
    /**
     * Gets query for [[Contratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratos()
    {
        return $this->hasMany(Contratos::class, ['idEmpresas' => 'id']);
    }

    /**
     * Gets query for [[EmpresasRutas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresasRutas()
    {
        return $this->hasMany(EmpresasRutas::class, ['idEmpresas' => 'id']);
    }

    /**
     * Gets query for [[IdRutas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdRutas()
    {
        return $this->hasMany(Rutas::class, ['id' => 'idRutas'])->viaTable('empresas_rutas', ['idEmpresas' => 'id']);
    }

    /**
     * Gets query for [[Maquinas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaquinas()
    {
        return $this->hasMany(Maquinas::class, ['idEmpresas' => 'id']);
    }
}
