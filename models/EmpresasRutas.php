<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empresas_rutas".
 *
 * @property int $idEmpresas
 * @property int $idRutas
 *
 * @property Empresas $idEmpresas0
 * @property Rutas $idRutas0
 */
class EmpresasRutas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresas_rutas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idEmpresas', 'idRutas'], 'required'],
            [['idEmpresas', 'idRutas'], 'integer'],
            [['idEmpresas', 'idRutas'], 'unique', 'targetAttribute' => ['idEmpresas', 'idRutas']],
            [['idEmpresas'], 'exist', 'skipOnError' => true, 'targetClass' => Empresas::class, 'targetAttribute' => ['idEmpresas' => 'id']],
            [['idRutas'], 'exist', 'skipOnError' => true, 'targetClass' => Rutas::class, 'targetAttribute' => ['idRutas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idEmpresas' => 'Id Empresas',
            'idRutas' => 'Id Rutas',
        ];
    }

    /**
     * Gets query for [[IdEmpresas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresas0()
    {
        return $this->hasOne(Empresas::class, ['id' => 'idEmpresas']);
    }

    /**
     * Gets query for [[IdRutas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdRutas0()
    {
        return $this->hasOne(Rutas::class, ['id' => 'idRutas']);
    }
}
