<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compras_productos".
 *
 * @property int|null $idCompras
 * @property int|null $idProductos
 *
 * @property Compras $idCompras0
 * @property Productos $idProductos0
 */
class ComprasProductos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compras_productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idCompras', 'idProductos'], 'integer'],
            [['idCompras'], 'exist', 'skipOnError' => true, 'targetClass' => Compras::class, 'targetAttribute' => ['idCompras' => 'id']],
            [['idProductos'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::class, 'targetAttribute' => ['idProductos' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idCompras' => 'Id Compras',
            'idProductos' => 'Id Productos',
        ];
    }

    /**
     * Gets query for [[IdCompras0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdCompras0()
    {
        return $this->hasOne(Compras::class, ['id' => 'idCompras']);
    }

    /**
     * Gets query for [[IdProductos0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProductos0()
    {
        return $this->hasOne(Productos::class, ['id' => 'idProductos']);
    }
}
