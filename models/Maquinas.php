<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maquinas".
 *
 * @property int $id
 * @property int $idEmpresas
 * @property string $numero_de_serie
 * @property string $nombre
 * @property string|null $tipo_de_maquina
 * @property int $estado
 * @property string|null $fecha_de_ultima_instalacion
 *
 * @property Contratos[] $contratos
 * @property Empresas $idEmpresas0
 * @property Productos[] $idProductos
 * @property Incidencias[] $incidencias
 * @property Mantenimientos[] $mantenimientos
 * @property MaquinasProductos[] $maquinasProductos
 * @property Ventas[] $ventas
 */
class Maquinas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    
    public $maquinasProductosList = [];
    
    public static function tableName()
    {
        return 'maquinas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_de_serie', 'nombre'], 'required'],
            [['idEmpresas', 'estado'], 'integer'],
            [['fecha_de_ultima_instalacion'], 'safe'],
            [['nombre', 'tipo_de_maquina'], 'string', 'max' => 60],
            [['numero_de_serie'], 'string', 'max' => 30],
            [['numero_de_serie'], 'unique'],
            [['numero_de_serie'], 'match', 'pattern' => '/^[a-zA-Z0-9]+$/', 'message' => 'El número de serie solo puede contener letras y números.'],
            [['idEmpresas'], 'exist', 'skipOnError' => true, 'targetClass' => Empresas::class, 'targetAttribute' => ['idEmpresas' => 'id']],
            [['maquinasProductosList'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idEmpresas' => 'Empresa',
            'numero_de_serie' => 'Número de Serie',
            'nombre' => 'Nombre',
            'tipo_de_maquina' => 'Tipo de Máquina',
            'estado' => 'Estado',
            'fecha_de_ultima_instalacion' => 'Fecha De Ultima Instalacion',
        ];
    }

    /**
     * Gets query for [[Contratos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContratos()
    {
        return $this->hasMany(Contratos::class, ['idMaquinas' => 'id']);
    }

    /**
     * Gets query for [[IdEmpresas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresas0()
    {
        return $this->hasOne(Empresas::class, ['id' => 'idEmpresas']);
    }

    /**
     * Gets query for [[IdProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdProductos()
    {
        return $this->hasMany(Productos::class, ['id' => 'idProductos'])->viaTable('maquinas_productos', ['idMaquinas' => 'id']);
    }

    /**
     * Gets query for [[Incidencias]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIncidencias()
    {
        return $this->hasMany(Incidencias::class, ['idMaquinas' => 'id']);
    }

    /**
     * Gets query for [[Mantenimientos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMantenimientos()
    {
        return $this->hasMany(Mantenimientos::class, ['idMaquinas' => 'id']);
    }

    /**
     * Gets query for [[MaquinasProductos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaquinasProductos()
    {
        return $this->hasMany(MaquinasProductos::class, ['idMaquinas' => 'id']);
    }

    /**
     * Gets query for [[Ventas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVentas()
    {
        return $this->hasMany(Ventas::class, ['idMaquinas' => 'id']);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        // Guardar los productos asociados a la máquina
        MaquinasProductos::deleteAll(['idMaquinas' => $this->id]);
        foreach ($this->maquinasProductosList as $maquinasProductoData) {
            $maquinasProducto = new MaquinasProductos();
            $maquinasProducto->idMaquinas = $this->id;
            $maquinasProducto->idProductos = $maquinasProductoData['idProductos'];
            $maquinasProducto->stock = $maquinasProductoData['stock'];
            $maquinasProducto->save();
        }
    }
    
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->idEmpresas) {
                $this->estado = 1;
            } else {
                $this->estado = 0;
            }
            return true;
        }
        return false;
    }

    /**
     * Get total incidences for the machine.
     *
     * @return int
     */
    public function getTotalIncidences()
    {
        return $this->getIncidencias()->count();
    }

    /**
     * Get total products for the machine.
     *
     * @return int
     */
    public function getTotalProducts()
    {
        return $this->getIdProductos()->count();
    }
    /**
 * Get total out of stock products for the machine.
 *
 * @return int
 */
public function getOutOfStockProducts()
{
    return $this->getMaquinasProductos()->where(['stock' => 0])->count();
}

}
