<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "rutas".
 *
 * @property int $id
 * @property string $nombre
 * @property string|null $descripcion
 * @property string $duracion
 * @property float $distancia_km
 *
 * @property EmpresasRutas[] $empresasRutas
 * @property Empresas[] $idEmpresas
 */
class Rutas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $empresasIds; // Nueva propiedad virtual
    
    public static function tableName()
    {
        return 'rutas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['descripcion'], 'string', 'max' => 255],
            [['duracion'], 'safe'],
            [['distancia_km'], 'number'],
            [['nombre'], 'string', 'max' => 60],
            [['empresasIds'], 'required'],
            [['nombre', 'empresasIds'], 'validateUniqueRoute'], // Add custom validation rule
        ];
    }

    /**
     * Custom validation method to prevent duplicate route names for the same companies.
     */
    public function validateUniqueRoute($attribute, $params, $validator)
    {
        $query = self::find()->where(['nombre' => $this->nombre]);

        if (!$this->isNewRecord) {
            $query->andWhere(['not', ['id' => $this->id]]);
        }

        $existingRoutes = $query->all();

        foreach ($existingRoutes as $route) {
            $existingEmpresasIds = ArrayHelper::getColumn($route->getIdEmpresas()->asArray()->all(), 'id');
            sort($existingEmpresasIds);
            sort($this->empresasIds);

            if ($existingEmpresasIds === $this->empresasIds) {
                $this->addError($attribute, 'Ya existe una ruta con el mismo nombre para las mismas empresas.');
                break;
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre de la Ruta',
            'descripcion' => 'Descripción',
            'duracion' => 'Duracion',
            'distancia_km' => 'Distancia Km',
            'empresasIds' => 'Empresas',
        ];
    }

    /**
     * Gets query for [[EmpresasRutas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresasRutas()
    {
        return $this->hasMany(EmpresasRutas::class, ['idRutas' => 'id']);
    }

    /**
     * Gets query for [[IdEmpresas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdEmpresas()
    {
        return $this->hasMany(Empresas::class, ['id' => 'idEmpresas'])->viaTable('empresas_rutas', ['idRutas' => 'id']);
    }
    
    public function getEmpresasList1()
    {
        $empresas = $this->idEmpresas;
        $empresasNames = [];

        foreach ($empresas as $empresa) {
            $empresasNames[] = $empresa->nombre;
        }

        return implode(', ', $empresasNames);
    }

    public function getEmpresasList()
    {
        return ArrayHelper::getColumn($this->getIdEmpresas()->asArray()->all(), 'id');
    }

    /**
     * Sobreescribe afterFind para cargar los IDs de las empresas asociadas a la ruta.
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->empresasIds = $this->getEmpresasList();
    }

    /**
     * Sobreescribe beforeSave para manejar la relación con las empresas.
     *
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // Aquí puedes manejar la lógica para guardar las relaciones con las empresas
            return true;
        }
        return false;
    }

    /**
     * Sobreescribe afterSave para manejar la relación con las empresas.
     *
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->saveEmpresas();
    }

    /**
     * Guarda las relaciones con las empresas.
     */
    protected function saveEmpresas()
    {
        EmpresasRutas::deleteAll(['idRutas' => $this->id]);
        if (is_array($this->empresasIds)) {
            foreach ($this->empresasIds as $empresaId) {
                $empresasRutas = new EmpresasRutas();
                $empresasRutas->idRutas = $this->id;
                $empresasRutas->idEmpresas = $empresaId;
                $empresasRutas->save();
            }
        }
    }
}
