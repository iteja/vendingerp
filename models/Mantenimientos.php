<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "mantenimientos".
 *
 * @property int $id
 * @property int $idMaquinas
 * @property string|null $descripcion
 * @property int $estado
 * @property string $fecha
 *
 * @property Maquinas $idMaquinas0
 */
class Mantenimientos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mantenimientos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idMaquinas', 'fecha'], 'required'],
            [['idMaquinas', 'estado'], 'integer'],
            [['descripcion'], 'string', 'max' => 255],
            [['fecha'], 'safe'],
            [['idMaquinas'], 'exist', 'skipOnError' => true, 'targetClass' => Maquinas::class, 'targetAttribute' => ['idMaquinas' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idMaquinas' => 'Máquina',
            'descripcion' => 'Descripción',
            'estado' => 'Estado',
            'fecha' => 'Fecha programada',
        ];
    }

    /**
     * Gets query for [[IdMaquinas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIdMaquinas0()
    {
        return $this->hasOne(Maquinas::class, ['id' => 'idMaquinas']);
    }
    
public function beforeSave($insert)
{
    if (parent::beforeSave($insert)) {
        // Verifica si el estado ha sido modificado manualmente
        if (!$this->isAttributeChanged('estado')) {
            // Solo actualiza el estado basado en la fecha si no ha sido modificado manualmente
            if (strtotime($this->fecha) <= strtotime(date('Y-m-d'))) {
                $this->estado = 0; // Completado
            }
        }
        return true;
    }
    return false;
}

}
