<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Compras $model */
/** @var yii\widgets\ActiveForm $form */
/** @var app\models\Productos[] $productos */

?>

<div class="empresas-form card shadow-sm p-4">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'nombre')->label('Nombre*')->textInput(['maxlength' => true, 'placeholder' => 'Ej: El Corte Inglés']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'industria')->label('Industria')->textInput(['maxlength' => true, 'placeholder' => 'Ej: Distribución']) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'ciudad')->label('Ciudad*')->textInput(['maxlength' => true, 'placeholder' => 'Ej: Santander']) ?>

    
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'direccion')->label('Dirección*')->textInput(['maxlength' => true, 'placeholder' => 'Ej: Av. Nueva Montaña']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'coordenadas')->label('Coordenadas*')->textInput(['maxlength' => true, 'placeholder' => 'Ej: -53.1525, 120.2807']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'representante')->label('Representante*')->textInput(['maxlength' => true, 'placeholder' => 'Ej: Marta Álvarez']) ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'telefono')->textInput(['maxlength' => true, 'oninput' => 'this.value = this.value.replace(/\D/g, "")', 'placeholder' => 'Ej: 644673245']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'correo')->textInput(['maxlength' => true, 'placeholder' => 'Ej: info@example.com']) ?>
        </div>
      
    </div>

    <div class="form-group text-right">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    .empresas-form .form-group {
        margin-bottom: 1rem;
    }
    .empresas-form .form-control {
        border-radius: 0.25rem;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .help-block {
    color: red;
    padding-left: 3px;
}

</style>
