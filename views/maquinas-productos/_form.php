<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Maquinas;
use app\models\Productos;

/* @var $this yii\web\View */
/* @var $model app\models\MaquinasProductos */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="maquinas-productos-form card shadow-sm p-4">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'idMaquinas')->label('Máquina*')->dropDownList(
                ArrayHelper::map(\app\models\Maquinas::find()->all(), 'id', function($maquina) {
                    return $maquina->nombre . ' - ' . $maquina->numero_de_serie;
                }), 
                ['prompt' => 'Seleccione una Máquina']
            ) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'idProductos')->label('Productos*')->listBox(
                ArrayHelper::map(Productos::find()->all(), 'id', 'nombre'),[
                'multiple' => true,
                'size' => 14, // Ajusta el tamaño según tus necesidades
                'class' => 'form-control',
            ])?>
        </div>
    </div>

    <div class="form-group text-right">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        const listBox = document.querySelector('.maquinas-productos-form .form-control[multiple]');
        listBox.addEventListener('click', function(e) {
            if (e.target.tagName === 'OPTION') {
                e.target.selected = !e.target.selected;
            }
        });

        // Evitar la selección múltiple predeterminada con arrastre
        listBox.addEventListener('mousedown', function(e) {
            e.preventDefault();
        });
    });
</script>

<style>
    .maquinas-productos-form .form-group {
        margin-bottom: 1rem;
    }
    .maquinas-productos-form .form-control {
        border-radius: 0.25rem;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .help-block {
        color: red;
        padding-left: 3px;
    }
    .maquinas-productos-form .text-right .btn {
        margin-top: 10px;
    }
    .producto-item {
        margin-bottom: 10px;
    }
</style>
