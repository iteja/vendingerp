<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\MaquinasProductos;


/** @var yii\web\View $this */
/** @var app\models\Maquinas $model */
/** @var yii\widgets\ActiveForm $form */

?>

<div class="maquinas-productos-form card shadow-sm p-4">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>

    <div class="row">
        <div class="col-md-6">
             <?= $form->field($model, 'idProductos')->label('Productos*')->dropDownList(
                ArrayHelper::map(\app\models\Productos::find()->all(), 'id', 'nombre'),
                     ['disabled' => true,] ,
                ['prompt' => 'Seleccione un Producto']
            ) ?>
            
        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'stock')->textInput(['type' => 'text', 'oninput' => 'this.value = this.value.replace(/\D/g, "")','placeholder' => 'Ej: 20']) ?>

        </div>
    </div>


    
   
    <div class="form-group text-right">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    .maquinas-productos-form .form-group {
        margin-bottom: 1rem;
    }
    .maquinas-productos-form .form-control {
        border-radius: 0.25rem;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .help-block {
        color: red;
        padding-left: 3px;
    }
    /* Alineación del botón */
    .maquinas-productos-form .text-right .btn {
        margin-top: 10px; /* Espacio superior */
    }
    .producto-item {
        margin-bottom: 10px;
    }
</style>
