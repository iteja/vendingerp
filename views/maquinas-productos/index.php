<?php

use app\models\MaquinasProductos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\search\ComprasSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Productos de ' . ($model ? $model->idMaquinas0->nombre : 'Desconocido');

?>
<div class="maquinas-productos-index container">
    <div class="d-flex justify-content-between align-items-center mb-3">
        <h1 class="mb-0"><?= Html::encode($this->title) ?></h1>
        <?= Html::a('Volver a máquinas', ['maquinas/index'], ['class' => 'btn btn-outline-secondary']) ?>
    </div>
    <div class="input-group mb-3" style="width: 450px;">
        <input type="text" id="maquinas-productos-search" class="form-control table-search" placeholder="Buscar...">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary btn-reset-search" type="button" id="reset-search">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>

    <div class="card shadow-sm p-4">
        <?php Pjax::begin(); ?>
        <div class="card-body p-0">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-hover mb-0', 'id' => 'maquinas-productos-table'],
                'columns' => [
                    [
                        'attribute' => 'idProductos0.nombre',
                        'label' => 'Producto',
                        'contentOptions' => ['style' => 'white-space: nowrap;'],
                    ],
                    [
                        'attribute' => 'idProductos0.codigo_barras',
                        'label' => 'Código de Barras',
                        'contentOptions' => ['style' => 'white-space: nowrap;'],
                    ],
                    'stock',
                    [
                        'class' => ActionColumn::className(),
                        'urlCreator' => function ($action, MaquinasProductos $model, $key, $index, $column) {
                            return Url::toRoute([$action, 'idMaquinas' => $model->idMaquinas, 'idProductos' => $model->idProductos]);
                        },
                        'contentOptions' => ['class' => 'text-right', 'style' => 'white-space: nowrap;'],
                        'buttonOptions' => ['class' => 'btn btn-sm btn-outline-secondary mr-2'],
                    ],
                ],
            ]); ?>
        </div>
        <?php Pjax::end(); ?>
    </div>
</div>

<style>
    @media (min-width: 1200px) {
        .container, .container-sm, .container-md, .container-lg, .container-xl {
            max-width: 3840px;
        }
    }
    .container {
        padding-right: 0;
    }
    .table-search {
        border: none;
        border-radius: 8px;
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
        padding: 8px 12px;
    }
    .content {
        background-color: #f8f9fa;
    }
    .summary {
        padding: 6px;
    }
    .maquinas-productos-index {
        background-color: #f8f9fa;
        padding: 20px;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .btn-reset-search {
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
    }
    .card .table thead th {
        background-color: #f1f1f1;
        border-bottom: 2px solid #dee2e6;
    }
    .card .table tbody tr:hover {
        background-color: #e9ecef;
    }
    .btn-primary {
        background-color: #0056b3;
        border-color: #0056b3;
    }
    .btn-primary:hover {
        background-color: #004494;
        border-color: #004494;
    }
    .table td, .table th {
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
        word-wrap: break-word;
    }
    .grid-view .action-column .btn {
        margin-right: 2px;
    }
    .btn-outline-secondary {
        border-color: #6c757d;
        color: #6c757d;
    }
    .btn-outline-secondary:hover {
        background-color: #6c757d;
        color: #fff;
    }
    .table td {
        max-width: 280px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: normal;
    }
    .text-right {
        text-align: right !important;
    }

    .stock-low {
        background-color: rgba(255, 0, 0, 0.1) !important;
    }
    .stock-low:hover {
        background-color: rgba(255, 0, 0, 0.2) !important;
    }
    .stock-high {
        background-color: rgba(0, 255, 0, 0.1) !important;
    }
    .stock-high:hover {
        background-color: rgba(0, 255, 0, 0.2) !important;
    }
    .td{
        border: 1px solid #dee2e6;
    }
</style>


<?php
$this->registerJs("
    document.getElementById('maquinas-productos-search').addEventListener('keyup', function() {
        var input, filter, table, tr, td, i, j, txtValue;
        input = document.getElementById('maquinas-productos-search');
        filter = input.value.toUpperCase();
        table = document.getElementById('maquinas-productos-table');
        tr = table.getElementsByTagName('tr');
        for (i = 1; i < tr.length; i++) {
            tr[i].style.display = 'none';
            td = tr[i].getElementsByTagName('td');
            for (j = 0; j < td.length; j++) {
                if (td[j]) {
                    txtValue = td[j].textContent || td[j].innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = '';
                        break;
                    }
                }
            }
        }
    });
    document.getElementById('reset-search').addEventListener('click', function() {
        document.getElementById('maquinas-productos-search').value = '';
        var table = document.getElementById('maquinas-productos-table');
        var tr = table.getElementsByTagName('tr');
        for (var i = 1; i < tr.length; i++) {
            tr[i].style.display = '';
        }
    });
");

$this->registerJs("
    $('#maquinas-productos-table td').each(function() {
        if (this.offsetWidth < this.scrollWidth) {
            $(this).attr('title', $(this).text());
            $(this).tooltip();
        }
    });

    $(document).ready(function() {
        $('#maquinas-productos-table tbody tr').each(function() {
            var stock = $(this).find('td:nth-child(3)').text().trim();
            if (parseInt(stock) === 0) {
                $(this).addClass('stock-low');
            } else if (parseInt(stock) > 0) {
                $(this).addClass('stock-high');
            }
        });
    });
");
?>
