<?php

use app\models\Compras;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\search\ComprasSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Compras';

?>
<div class="compras-index container">

    <div class="d-flex justify-content-between align-items-center mb-3">
        <h1 class="mb-0"><?= Html::encode($this->title) ?></h1>
        <?= Html::a('Nueva Compra', ['create'], ['class' => 'btn btn-primary']) ?>
    </div>
    <div class="input-group mb-3" style="width: 450px;">
        <input type="text" id="compras-search" class="form-control table-search" style="width: 50px;" placeholder="Buscar...">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary btn-reset-search" type="button" id="reset-search">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>

    <div class="card shadow-sm p-4">
        <?php Pjax::begin(); ?>
        <div class="card-body p-0">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-hover mb-0', 'id' => 'compras-table'],
                'columns' => [
                    [
                        'attribute' => 'idProductos0.nombre',
                        'label' => 'Producto',
                        'contentOptions' => ['style' => 'white-space: nowrap;'],
                    ],
                    [
                        'attribute' => 'idProductos0.codigo_barras',
                        'label' => 'Código de Barras',
                        'contentOptions' => ['style' => 'white-space: nowrap;'],
                    ],
                    [
                        'label' => 'Proveedor',
                        'value' => function ($model) {
                            return !empty($model->proveedor) ? $model->proveedor : '-';
                        },
                    ],
                    [
                        'attribute' => 'cantidad',
                        'value' => function ($model) {
                            return $model->getFormattedCantidad();
                        }
                    ],
                    [
                        'label' => 'Precio Unitario',
                        'value' => function ($model) {
                            $precioUnitario = $model->precio / $model->cantidad;
                            $precioUnitarioRedondeado = ceil($precioUnitario * 100) / 100;
                            return Yii::$app->formatter->asDecimal($precioUnitarioRedondeado, 2) . ' €';
                        },
                        'contentOptions' => ['style' => 'white-space: nowrap;'],
                    ],
                    [
                        'attribute' => 'precio',
                        'label' => 'Precio Total',
                        'contentOptions' => ['style' => 'white-space: nowrap;'],
                        'value' => function ($model) {
                            return Yii::$app->formatter->asDecimal($model->precio, 2) . ' €';
                        },
                    ],
                    [
                        'attribute' => 'fecha',
                        'format' => ['date', 'php:d-m-Y'],
                        'contentOptions' => ['style' => 'white-space: nowrap;'],
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'urlCreator' => function ($action, Compras $model, $key, $index, $column) {
                            return Url::toRoute([$action, 'id' => $model->id]);
                        },
                        'contentOptions' => ['class' => 'text-right'], // Alineación a la derecha
                        'buttonOptions' => ['class' => 'btn btn-sm btn-outline-secondary mr-2'],
                        'template' => '{view} {update} {delete}',
                    ],
                ],
            ]); ?>
        </div>
        <?php Pjax::end(); ?>
    </div>
</div>

<style>
    @media (min-width: 1200px) {
        .container, .container-sm, .container-md, .container-lg, .container-xl {
            max-width: 3840px;
        }
    }
    
    .container {
        padding-right: 0;
    }
    .table-search {
        border: none;
        border-radius: 8px;
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
        padding: 8px 12px;
    }
    .content {
        background-color: #f8f9fa;
    }
    .summary {
        padding: 6px;
    }
    .compras-index {
        background-color: #f8f9fa;
        padding: 20px;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .btn-reset-search {
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
    }
    .card .table thead th {
        background-color: #f1f1f1;
        border-bottom: 2px solid #dee2e6;
    }
    .card .table tbody tr:hover {
        background-color: #e9ecef;
    }
    .btn-primary {
        background-color: #0056b3;
        border-color: #0056b3;
    }
    .btn-primary:hover {
        background-color: #004494;
        border-color: #004494;
    }
    .table td, .table th {
        white-space: normal; /* Permite el salto de línea */
        text-overflow: ellipsis; /* Agrega puntos suspensivos al texto que se desborda */
        overflow: hidden; /* Oculta el texto que se desborda de la celda */
        word-wrap: break-word; /* Permite que las palabras largas se dividan y ajusten */
    }
    .grid-view .action-column .btn {
        margin-right: 2px;
    }
    .btn-outline-secondary {
        border-color: #6c757d;
        color: #6c757d;
    }
    .btn-outline-secondary:hover {
        background-color: #6c757d;
        color: #fff;
    }
    div.compras-index input.table-search {
        width: 50px;
    }
    .table td {
        max-width: 200px; /* Define un ancho máximo para las celdas */
        overflow: hidden; /* Oculta el texto que se desborda de la celda */
        text-overflow: ellipsis; /* Agrega puntos suspensivos (...) al texto que se desborda */
        white-space: normal; /* Permite el salto de línea */
    }
</style>

<?php
$this->registerJs("
    document.getElementById('compras-search').addEventListener('keyup', function() {
        var input, filter, table, tr, td, i, j, txtValue;
        input = document.getElementById('compras-search');
        filter = input.value.toUpperCase();
        table = document.getElementById('compras-table');
        tr = table.getElementsByTagName('tr');
        for (i = 1; i < tr.length; i++) {
            tr[i].style.display = 'none';
            td = tr[i].getElementsByTagName('td');
            for (j = 0; j < td.length; j++) {
                if (td[j]) {
                    txtValue = td[j].textContent || td[j].innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = '';
                        break;
                    }
                }
            }
        }
    });

    document.getElementById('reset-search').addEventListener('click', function() {
        document.getElementById('compras-search').value = '';
        var table = document.getElementById('compras-table');
        var tr = table.getElementsByTagName('tr');
        for (var i = 1; i < tr.length; i++) {
            tr[i].style.display = '';
        }
    });
");

$this->registerJs("
    $('#compras-table td').each(function() {
        if (this.offsetWidth < this.scrollWidth) {
            $(this).attr('title', $(this).text());
            $(this).tooltip();
        }
    });
");
?>
