<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Compras $model */
/** @var yii\widgets\ActiveForm $form */
/** @var app\models\Productos[] $productos */

?>

<div class="compras-form card shadow-sm p-4">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'idProductos')->label('Producto*')->dropDownList(
                ArrayHelper::map($productos, 'id', function($producto) {
                    return $producto->nombre . ' - ' . $producto->codigo_barras;
                }), 
                ['prompt' => 'Seleccione un Producto']
            ) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'proveedor')->label('Proveedor')->textInput(['maxlength' => true, 'placeholder' => 'Ej: Arbesú.com']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'cantidad')->label('Cantidad*')->textInput(['maxlength' => 8, 'type' => 'text', 'oninput' => 'this.value = this.value.replace(/\D/g, "")', 'placeholder' => 'Ej: 20']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'precio')->label('Precio Total*')->textInput(['maxlength' => 11, 'placeholder' => 'Ej: 16.99']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'fecha')->label('Fecha*')->textInput(['type' => 'date', 'max' => date('Y-m-d')]) ?>
        </div>
    </div>

    <div class="form-group text-right">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    .compras-form .form-group {
        margin-bottom: 1rem;
    }
    .compras-form .form-control {
        border-radius: 0.25rem;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .help-block {
        color: red;
        padding-left: 3px;
    }
</style>
