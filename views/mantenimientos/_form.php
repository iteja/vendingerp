<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Compras $model */
/** @var yii\widgets\ActiveForm $form */
/** @var app\models\Productos[] $productos */

?>

<div class="mantenimientos-form card shadow-sm p-4">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'idMaquinas')->label('Máquina*')->dropDownList(
                ArrayHelper::map(\app\models\Maquinas::find()->all(), 'id', function($maquina) {
                    return $maquina->nombre . ' - ' . $maquina->numero_de_serie;
                }), 
                ['prompt' => 'Seleccione una Máquina']
            ) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'descripcion')->label('Descripción')->textarea(['maxlength' => true, 'rows' => 1, 'placeholder' => 'Ej: Mantenimiento preventivo del dispensador de monedas']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'fecha')->label('Fecha*')->input('date') ?>
        </div>
    </div>

    <div class="form-group text-right">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    .mantenimientos-form .form-group {
        margin-bottom: 1rem;
    }
    .mantenimientos-form .form-control {
        border-radius: 0.25rem;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .help-block {
    color: red;
    padding-left: 3px;
}

</style>



