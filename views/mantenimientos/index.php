<?php

use app\models\Mantenimientos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\search\MantenimientosSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Mantenimientos';

?>
<div class="mantenimientos-index container">

    <div class="d-flex justify-content-between align-items-center mb-3">
        <h1 class="mb-0"><?= Html::encode($this->title) ?></h1>
        <?= Html::a('Nuevo Mantenimiento', ['create'], ['class' => 'btn btn-primary']) ?>
    </div>
    <div class="input-group mb-3" style="width: 450px;">
        <input type="text" id="mantenimientos-search" class="form-control table-search" placeholder="Buscar...">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary btn-reset-search" type="button" id="reset-search">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>

    <div class="card shadow-sm p-4">
        <?php Pjax::begin(); ?>
        <div class="card-body p-0">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-hover mb-0', 'id' => 'mantenimientos-table'],
                'rowOptions' => function($model) {
                    return $model->estado ? ['class' => 'estado-completado'] : ['class' => 'estado-pendiente'];
                },
                'columns' => [
                    [
                        'attribute' => 'idMaquinas',
                        'value' => 'idMaquinas0.nombre',
                        'label' => 'Máquina'
                    ],
                    [
                        'attribute' => 'idMaquinas',
                        'value' => 'idMaquinas0.numero_de_serie',
                        'label' => 'Número de Serie'
                    ],
                    [
                        'attribute' => 'descripcion',
                        'format' => 'ntext',
                        'value' => function ($model) {
                            return !empty($model->descripcion) ? $model->descripcion : '-';
                        }
                    ],
                    [
                        'attribute' => 'estado',
                        'format' => 'raw',
                        'value' => function ($model) {
                            $checked = $model->estado ? 'checked' : '';
                            $id = 'estado-' . $model->id;
                            return Html::checkbox('estado', $model->estado, [
                                'id' => $id,
                                'class' => 'estado-checkbox',
                                'data-id' => $model->id,
                            ]);
                        },
                        'label' => 'Estado'
                    ],
                    [
                        'attribute' => 'fecha',
                        'format' => ['date', 'php:d-m-Y'],
                    ],
                    [
                        'class' => ActionColumn::class,
                        'urlCreator' => function ($action, Mantenimientos $model, $key, $index, $column) {
                            return Url::toRoute([$action, 'id' => $model->id]);
                        },
                        'contentOptions' => ['class' => 'text-right'], // Alineación a la derecha
                        'buttonOptions' => ['class' => 'btn btn-sm btn-outline-secondary mr-2'],
                    ],
                ],
            ]); ?>
        </div>
        <?php Pjax::end(); ?>
    </div>
</div>

<style>
    @media (min-width: 1200px) {
        .container, .container-sm, .container-md, .container-lg, .container-xl {
            max-width: 3840px;
        }
    }
    .container {
        padding-right: 0;
    }
    .table-search {
        border: none;
        border-radius: 8px;
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
        padding: 8px 12px;
    }
    .content {
        background-color: #f8f9fa;
    }
    .summary {
        padding: 6px;
    }
    .mantenimientos-index {
        background-color: #f8f9fa;
        padding: 20px;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .btn-reset-search {
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
    }
    .card .table thead th {
        background-color: #f1f1f1;
        border-bottom: 2px solid #dee2e6;
    }
    .card .table tbody tr:hover {
        background-color: #e9ecef;
    }
    .btn-primary {
        background-color: #0056b3;
        border-color: #0056b3;
    }
    .btn-primary:hover {
        background-color: #004494;
        border-color: #004494;
    }
    .table td, .table th {
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
        word-wrap: break-word;
    }
    .grid-view .action-column .btn {
        margin-right: 2px;
    }
    .btn-outline-secondary {
        border-color: #6c757d;
        color: #6c757d;
    }
    .btn-outline-secondary:hover {
        background-color: #6c757d;
        color: #fff;
    }
    div.mantenimientos-index input.table-search {
        width: 50px;
    }
    .table td {
        max-width: 280px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: normal;
    }
    .text-right {
        text-align: right !important;
    }

    .estado-completado {
        background-color: rgba(0, 255, 0, 0.1) !important; /* Verde claro */
    }
    .estado-completado:hover {
        background-color: rgba(0, 255, 0, 0.2) !important; /* Verde más oscuro al pasar el mouse */
    }
    .estado-pendiente {
        background-color: rgba(255, 0, 0, 0.1) !important; /* Rojo claro */
    }
    .estado-pendiente:hover {
        background-color: rgba(255, 0, 0, 0.2) !important; /* Rojo más oscuro al pasar el mouse */
    }
</style>

<?php
$this->registerJs("
    document.getElementById('mantenimientos-search').addEventListener('keyup', function() {
        var input, filter, table, tr, td, i, j, txtValue;
        input = document.getElementById('mantenimientos-search');
        filter = input.value.toUpperCase();
        table = document.getElementById('mantenimientos-table');
        tr = table.getElementsByTagName('tr');
        for (i = 1; i < tr.length; i++) {
            tr[i].style.display = 'none';
            td = tr[i].getElementsByTagName('td');
            for (j = 0; j < td.length; j++) {
                if (td[j]) {
                    txtValue = td[j].textContent || td[j].innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = '';
                        break;
                    }
                }
            }
        }
    });
    document.getElementById('reset-search').addEventListener('click', function() {
        document.getElementById('mantenimientos-search').value = '';
        var table = document.getElementById('mantenimientos-table');
        var tr = table.getElementsByTagName('tr');
        for (var i = 1; i < tr.length; i++) {
            tr[i].style.display = '';
        }
    });

    // Cambiar color de la fila al hacer clic en la casilla de verificación
    $('.estado-checkbox').on('change', function() {
        var isChecked = $(this).is(':checked');
        var row = $(this).closest('tr');
        if (isChecked) {
            row.removeClass('estado-pendiente').addClass('estado-completado');
        } else {
            row.removeClass('estado-completado').addClass('estado-pendiente');
        }

        // Actualizar el estado en la base de datos mediante una llamada AJAX
        var id = $(this).data('id');
        $.ajax({
            url: '" . Url::to(['mantenimientos/update-estado']) . "',
            type: 'POST',
            data: {
                id: id,
                estado: isChecked ? 1 : 0,
                _csrf: yii.getCsrfToken()
            },
            success: function(response) {
                if (response.success) {
                    console.log('Estado actualizado correctamente.');
                } else {
                    console.error('Error al actualizar el estado.');
                }
            },
            error: function(xhr, status, error) {
                console.error('Error al actualizar el estado:', error);
            }
        });
    });
");
?>
