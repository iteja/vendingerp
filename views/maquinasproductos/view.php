<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\MaquinasProductos $model */

$this->title = $model->idMaquinas;
$this->params['breadcrumbs'][] = ['label' => 'Maquinas Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="maquinas-productos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'idMaquinas' => $model->idMaquinas, 'idProductos' => $model->idProductos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'idMaquinas' => $model->idMaquinas, 'idProductos' => $model->idProductos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idMaquinas',
            'idProductos',
            'stock',
        ],
    ]) ?>

</div>
