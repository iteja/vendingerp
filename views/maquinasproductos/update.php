<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MaquinasProductos $model */

$this->title = 'Update Maquinas Productos: ' . $model->idMaquinas;
$this->params['breadcrumbs'][] = ['label' => 'Maquinas Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idMaquinas, 'url' => ['view', 'idMaquinas' => $model->idMaquinas, 'idProductos' => $model->idProductos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="maquinas-productos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
