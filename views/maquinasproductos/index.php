<?php

use app\models\MaquinasProductos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;
/** @var yii\web\View $this */
/** @var app\models\search\MaquinasProductosSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Maquinas Productos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maquinas-productos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Maquinas Productos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idMaquinas',
            'idProductos',
            'stock',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, MaquinasProductos $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'idMaquinas' => $model->idMaquinas, 'idProductos' => $model->idProductos]);
                 }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
