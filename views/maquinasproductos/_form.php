<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\MaquinasProductos $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="maquinas-productos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idMaquinas')->textInput() ?>

    <?= $form->field($model, 'idProductos')->textInput() ?>

    <?= $form->field($model, 'stock')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
