<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\MaquinasProductos $model */

$this->title = 'Create Maquinas Productos';
$this->params['breadcrumbs'][] = ['label' => 'Maquinas Productos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maquinas-productos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
