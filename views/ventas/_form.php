<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Compras $model */
/** @var yii\widgets\ActiveForm $form */
/** @var app\models\Productos[] $productos */

?>

<div class="ventas-form card shadow-sm p-4">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'idMaquinas')->label('Máquina*')->dropDownList(
                ArrayHelper::map(\app\models\Maquinas::find()->all(), 'id', function($maquina) {
                    return $maquina->nombre . ' - ' . $maquina->numero_de_serie;
                }), 
                ['prompt' => 'Seleccione una Máquina']
            ) ?>
        </div>
        <div class="col-md-6">
           
            <?= $form->field($model, 'idProductos')->label('Producto*')->dropDownList(
                ArrayHelper::map(\app\models\Productos::find()->all(), 'id', function($producto) {
                    return $producto->nombre . ' - ' . $producto->codigo_barras;
                }), 
                ['prompt' => 'Seleccione un Producto']
            ) ?>

        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'metodo_de_pago')->label('Metodo de Pago*')->dropDownList(
                ['Efectivo' => 'Efectivo', 'Tarjeta' => 'Tarjeta'],
                ['prompt' => 'Seleccione un Método de Pago']
            ) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'precio')->label('Precio de Venta Unitario*')->textInput(['maxlength' => 11, 'placeholder' => 'Ej: 16.99']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
             <?= $form->field($model, 'cantidad')->label('Cantidad Vendida*')->textInput(['maxlength' => 8, 'type' => 'text', 'oninput' => 'this.value = this.value.replace(/\D/g, "")','placeholder' => 'Ej: 20']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'fecha')->label('Fecha de Venta*')->textInput(['type' => 'date', 'max' => date('Y-m-d')]) ?>
        </div>
    </div>

    <div class="form-group text-right">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>
    

    <?php ActiveForm::end(); ?>

</div>

<style>
    .ventas-form .form-group {
        margin-bottom: 1rem;
    }
    .ventas-form .form-control {
        border-radius: 0.25rem;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .help-block {
    color: red;
    padding-left: 3px;
}

</style>
