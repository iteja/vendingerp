<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon2.png')]);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">

    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <style>
         /* Media query para resoluciones mayores a 1080p */
        @media screen and (min-height: 1080px) {
            .nav-link {
                padding-top: 24px; /* Ajusta según tus necesidades */
                padding-bottom: 24px; /* Ajusta según tus necesidades */
            }
        }

        /* Media query para resoluciones mayores a 4k */
        @media screen and (min-height: 1440px) {
            .nav-link {
                padding-top: 32px; /* Ajusta según tus necesidades */
                padding-bottom: 32px; /* Ajusta según tus necesidades */
            }
        }
        .h-100 {
            background-color: #F8F9FA;
        }
        body {
            display: flex;
            height: 100vh;
            overflow: auto;
            margin: 0;
            padding: 0;
        }
        .navbar {
            background-color: #F1F1F1;
            z-index: 1000;
        }
        .navbar {
            height: 56px;
        }
        .sidebar {
            height: 100vh;
            position: fixed;
            top: 56px; /* Altura de la barra superior */
            left: 0;
            width: 200px;
            background-color: #003c71;
            padding-top: 20px;
            overflow-x: hidden;
            overflow-y: auto;
            color: #fff;
            transition: transform 0.3s ease; /* Transición para la animación de deslizamiento */
        }
        .sidebar .nav-link {
            color: white;
            font-size: 15px;
            padding-left: 20px; /* Agregando espacio entre el icono y el nombre */
            transition: color 0.3s ease; /* Transición suave al cambiar el color */
        }
        .sidebar .nav-link .icon-container {
            display: inline-block;
            width: 30px; /* Ancho fijo del contenedor */
            vertical-align: middle;
        }
        .sidebar .nav-link .icon {
            font-size: 1rem; /* Tamaño de los iconos */
            margin-right: 10px; /* Espacio entre el icono y el texto */
        }
        .sidebar .nav-link .text {
            display: inline-block;
            vertical-align: middle;
        }
        .sidebar .nav-link:hover {
            color: #0288D1;
        }
        .sidebar .nav-link.active {
            color: #0288D1;
        }
        .content {
            margin-left: 200px;
            padding: 20px;
            width: calc(100% - 200px);
            padding-top: 56px; /* Espacio para la barra superior */
        }
        .navbar-brand img {
            height: 35px;
            padding-top: 0px;
        }
        .container, .container-fluid, .container-sm, .container-md, .container-lg, .container-xl {
            margin-left: 0;
            padding-left: 0;
        }
        .nav-item {
            padding-bottom: 21px;
        }
        a:hover {
            color: #004494;
            text-decoration: underline;
        }
        a {
            color: #004494;
            text-decoration: none;
            background-color: transparent;
        }
        th {
            color: #004494;
        }
    </style>
</head>
<body>
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('@web/logo2.png', ['alt' => Yii::$app->name]),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark fixed-top',
        ],
    ]);
    NavBar::end();
    ?>
</header>

<div class="sidebar">
    <?php
    echo Nav::widget([
        'options' => ['class' => 'nav flex-column'],
        'items' => [
            [
                'label' => '<span class="icon-container"><i class="fas fa-tachometer-alt icon"></i></span><span class="text">Dashboard</span>',
                'url' => ['/site/index'],
                'encode' => false,
                'linkOptions' => ['class' => Yii::$app->controller->id == 'site' ? 'nav-link active' : 'nav-link'],
            ],
            [
                'label' => '<span class="icon-container"><i class="fas fa-shopping-cart icon"></i></span><span class="text">Compras</span>',
                'url' => ['/compras/index'],
                'encode' => false,
                'linkOptions' => ['class' => Yii::$app->controller->id == 'compras' ? 'nav-link active' : 'nav-link'],
            ],
            [
                'label' => '<span class="icon-container"><i class="fas fa-file-signature icon"></i></span><span class="text">Contratos</span>',
                'url' => ['/contratos/index'],
                'encode' => false,
                'linkOptions' => ['class' => Yii::$app->controller->id == 'contratos' ? 'nav-link active' : 'nav-link'],
            ],
            [
                'label' => '<span class="icon-container"><i class="fas fa-city icon"></i></span><span class="text">Empresas</span>',
                'url' => ['/empresas/index'],
                'encode' => false,
                'linkOptions' => ['class' => Yii::$app->controller->id == 'empresas' ? 'nav-link active' : 'nav-link'],
            ],
            [
                'label' => '<span class="icon-container"><i class="fa-solid fa-mobile-screen fa-2x icon" style="font-size: 1.25rem;"></i></span><span class="text">Máquinas</span>',
                'url' => ['/maquinas/index'],
                'encode' => false,
                'linkOptions' => ['class' => Yii::$app->controller->id == 'maquinas' ? 'nav-link active' : 'nav-link'],
            ],
            [
                'label' => '<span class="icon-container"><i class="fas fa-exclamation-triangle icon"></i></span><span class="text">Incidencias</span>',
                'url' => ['/incidencias/index'],
                'encode' => false,
                'linkOptions' => ['class' => Yii::$app->controller->id == 'incidencias' ? 'nav-link active' : 'nav-link'],
            ],
            [
                'label' => '<span class="icon-container"><i class="fas fa-wrench icon"></i></span><span class="text">Mantenimientos</span>',
                'url' => ['/mantenimientos/index'],
                'encode' => false,
                'linkOptions' => ['class' => Yii::$app->controller->id == 'mantenimientos' ? 'nav-link active' : 'nav-link'],
            ],
            [
                'label' => '<span class="icon-container"><i class="fas fa-box icon"></i></span><span class="text">Productos</span>',
                'url' => ['/productos/index'],
                'encode' => false,
                'linkOptions' => ['class' => Yii::$app->controller->id == 'productos' ? 'nav-link active' : 'nav-link'],
            ],
            [
                'label' => '<span class="icon-container"><i class="fas fa-chart-line icon"></i></span><span class="text">Ventas</span>',
                'url' => ['/ventas/index'],
                'encode' => false,
                'linkOptions' => ['class' => Yii::$app->controller->id == 'ventas' ? 'nav-link active' : 'nav-link'],
            ],
            [
                'label' => '<span class="icon-container"><i class="fas fa-route icon"></i></span><span class="text">Rutas</span>',
                'url' => ['/rutas/index'],
                'encode' => false,
                'linkOptions' => ['class' => Yii::$app->controller->id == 'rutas' ? 'nav-link active' : 'nav-link'],
            ],
        ],
    ]);
    ?>
</div>

<div class="content">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
