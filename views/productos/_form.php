<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Productos $model */
/** @var yii\widgets\ActiveForm $form */

$this->title = 'Crear Producto';

?>

<div class="productos-form card shadow-sm p-4">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'nombre')->label('Nombre*')->textInput(['maxlength' => true, 'placeholder' => 'Ej: Powerade']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'codigo_barras')->label('Código de Barras*')->textInput(['maxlength' => true, 'placeholder' => 'Ej: 001122334455']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'descripcion')->label('Descripción')->textarea(['maxlength' => true, 'rows' => 1, 'placeholder' => 'Ej: Bebida isotónica con tapón Sport']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'categoria')->label('Categoria*')->textInput(['maxlength' => true, 'placeholder' => 'Ej: Bebidas']) ?>
        </div>
    </div>

    <div class="form-group text-right">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    .productos-form .form-group {
        margin-bottom: 1rem;
    }
    .productos-form .form-control {
        border-radius: 0.25rem;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .help-block {
    color: red;
    padding-left: 3px;
}
</style>

