<?php

$this->title = 'Dashboard';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>

    <!-- Enlaza a Highcharts y Highcharts 3D desde CDN -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <?php $this->registerCssFile('https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css'); ?>
    <?php $this->registerCssFile('@web/css/custom.css'); // Enlazar un archivo CSS personalizado si existe ?>

    <style>
        .chart-container {
            margin: 20px 0;
        }
        .chart {
            height: 400px;
            width: 100%;
            background-color: transparent;
        }
        .dashboard-title {
            margin-bottom: 20px;
            color: #343a40;
        }
        .chart-title {
            margin-bottom: 10px;
            color: #004494;
        }
        .content {
            background-color: #f8f9fa;
        }
        .container{
            margin: 0;
        }
        /* En custom.css */
        .custom-col-wide {
            width: 50%;
        }
            .container, .container-sm, .container-md, .container-lg, .container-xl {
        max-width: 3840px;
        max-height: 1440px;
        }


    </style>
</head>
<body>
    <div class="container-fluid">
        <div class="row chart-container">
            <div class="custom-col-wide">
                <h2 class="text-center chart-title">Máquinas con más Ventas</h2>
                <div id="ventasBarChart3D" class="chart card shadow-sm"></div>
            </div>
            <div class="custom-col-wide">
                <h2 class="text-center chart-title">Productos Más Vendidos</h2>
                <div id="productosMasVendidosPieChart3D" class="chart card shadow-sm"></div>
            </div>
        </div>

        <div class="row chart-container">
            <div class="custom-col-wide">
                <h2 class="text-center chart-title">Máquinas con menos Ventas</h2>
                <div id="maquinasMenosVentasBarChart3D" class="chart card shadow-sm"></div>
            </div>
            <div class="custom-col-wide">
                <h2 class="text-center chart-title">Productos con menos Ventas</h2>
                <div id="productosMenosVentasPieChart3D" class="chart card shadow-sm"></div>
            </div>
        </div>

        <div style="padding-bottom:30px;" class="row chart-container">

            <div class="custom-col-wide">
                <h2 class="text-center chart-title">Máquinas con más Incidencias</h2>
                <div id="incidenciasBarChart3D" class="chart card shadow-sm"></div>
            </div>
            <div class="custom-col-wide">
                <h2 class="text-center chart-title">Piezas Más Reemplazadas</h2>
                <div id="reemplazadasPieChart3D" class="chart card shadow-sm" ></div>
            </div>
        </div>
    </div>

    <script>
        // Configuración común para los gráficos
        const commonChartOptions = {
            chart: {
                backgroundColor: 'transparent',
                options3d: {
                    enabled: true,
                    alpha: 0,
                    beta: 0,
                    depth: 20,
                    viewDistance: 25
                },
                borderWidth: 0,
                plotBackgroundColor: 'transparent'
            },
            title: {
                text: null
            },
            exporting: {
                enabled: true
            },
            legend: {
                enabled: false
            },
            credits: {
                enabled: false
            }
        };

        // Gráfico de Columnas 3D - Máquinas con más Ventas
        Highcharts.chart('ventasBarChart3D', Highcharts.merge(commonChartOptions, {
            chart: {
                type: 'column'
            },
            xAxis: {
                categories: <?= json_encode(array_column($ventasData, 'nombre')) ?>
            },
            yAxis: {
                title: {
                    text: 'Número de Ventas'
                }
            },
            series: [{
                name: 'Ventas',
                data: <?= json_encode(array_map('intval', array_column($ventasData, 'numero_ventas'))) ?>,
                colorByPoint: true
            }]
        }));

        // Gráfico de Pie 3D - Productos Más Vendidos
        Highcharts.chart('productosMasVendidosPieChart3D', Highcharts.merge(commonChartOptions, {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            series: [{
                name: 'Ventas',
                data: <?= json_encode(array_map(function($item) {
                    return ['name' => $item['nombre'], 'y' => (int) $item['numero_ventas']];
                }, $productosData)) ?>
            }]
        }));

        // Gráfico de Columnas 3D - Máquinas con menos Ventas
        Highcharts.chart('maquinasMenosVentasBarChart3D', Highcharts.merge(commonChartOptions, {
            chart: {
                type: 'column'
            },
            xAxis: {
                categories: <?= json_encode(array_column($maquinasMenosVentasData, 'nombre')) ?>
            },
            yAxis: {
                title: {
                    text: 'Número de Ventas'
                }
            },
            series: [{
                name: 'Ventas',
                data: <?= json_encode(array_map('intval', array_column($maquinasMenosVentasData, 'numero_ventas'))) ?>,
                colorByPoint: true
            }]
        }));

        // Gráfico de Pie 3D - Productos con menos Ventas
        Highcharts.chart('productosMenosVentasPieChart3D', Highcharts.merge(commonChartOptions, {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            series: [{
                name: 'Ventas',
                data: <?= json_encode(array_map(function($item) {
                    return ['name' => $item['nombre'], 'y' => (int) $item['numero_ventas']];
                }, $productosMenosVentasData)) ?>
            }]
        }));

        // Gráfico de Columnas 3D - Máquinas con más Incidencias
        Highcharts.chart('incidenciasBarChart3D', Highcharts.merge(commonChartOptions, {
            chart: {
                type: 'column'
            },
            xAxis: {
                categories: <?= json_encode(array_column($incidenciasData, 'nombre')) ?>
            },
            yAxis: {
                title: {
                    text: 'Número de Incidencias'
                }
            },
            series: [{
                name: 'Incidencias',
                data: <?= json_encode(array_map('intval', array_column($incidenciasData, 'numero_incidencias'))) ?>,
                colorByPoint: true
            }]
        }));

        // Gráfico de Pie 3D - Piezas Más Reemplazadas
        Highcharts.chart('reemplazadasPieChart3D', Highcharts.merge(commonChartOptions, {
            chart: {
                type: 'pie',
                options3d: {
                    enabled: true,
                    alpha: 45,
                    beta: 0
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    depth: 35,
                    dataLabels: {
                        enabled: true,
                        format: '{point.name}'
                    }
                }
            },
            series: [{
                name: 'Reemplazos',
                data: <?= json_encode(array_map(function($item) {
                    return ['name' => $item['piezas_reemplazadas'], 'y' => (int) $item['veces_reemplazadas']];
                }, $reemplazadasData)) ?>
            }]
        }));
    </script>
</body>
</html>
