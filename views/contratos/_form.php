<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Compras $model */
/** @var yii\widgets\ActiveForm $form */
/** @var app\models\Productos[] $productos */

?>

<div class="contratos-form card shadow-sm p-4">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'idMaquinas')->label('Máquina*')->dropDownList(
                ArrayHelper::map(\app\models\Maquinas::find()->all(), 'id', function($maquina) {
                    return $maquina->nombre . ' - ' . $maquina->numero_de_serie;
                }), 
                ['prompt' => 'Seleccione una Máquina']
            ) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'idEmpresas')->label('Empresa*')->dropDownList(
                \yii\helpers\ArrayHelper::map(\app\models\Empresas::find()->all(), 'id', 'nombre'),
                ['prompt' => 'Seleccione una Empresa']
            ) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'tipo')->label('Tipo de contrato*')->textInput(['maxlength' => true, 'placeholder' => 'Ej: Porcentaje']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'descripcion')->textarea(['maxlength' => true, 'rows' => 1, 'placeholder' => 'Ej: Contrato de un 10% de las ventas']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'fecha_de_inicio')->label('Fecha de Inicio*')->input('date') ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'fecha_fin')->label('Fecha Fin*')->input('date') ?>
        </div>
    </div>

    <div class="form-group text-right">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    .contratos-form .form-group {
        margin-bottom: 1rem;
    }
    .contratos-form .form-control {
        border-radius: 0.25rem;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .help-block {
        color: red;
        padding-left: 3px;
    }
    /* Alineación del botón */
    .contratos-form .text-right .btn {
        margin-top: 10px; /* Espacio superior */
    }
</style>
