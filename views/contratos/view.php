<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Compras $model */

$this->title = 'Contrato ID: ' . $model->id;
\yii\web\YiiAsset::register($this);
?>

<div class="contratos-view container">

    <div class="d-flex justify-content-between align-items-center mb-3">
        <h1 class="mb-0"><?= Html::encode($this->title) ?></h1>
        <?= Html::a('Volver a la lista', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <div class="card shadow-sm p-4">
        <div class="d-flex justify-content-between mb-3">
            <div>
                <!-- Aquí estaba el botón de Exportar a PDF anteriormente -->
            </div>
            <div>
                <?= Html::a('Exportar a PDF', ['contratos/export-pdf', 'id' => $model->id], [
                    'class' => 'btn btn-outline-secondary mr-2',
                    'title' => 'Exportar a PDF',
                    'target' => '_blank',
                ]) ?>
                <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary mr-2']) ?>
                <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => '¿Estás seguro de que deseas eliminar este elemento?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
        </div>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'idMaquinas0.nombre',
                    'label' => 'Máquina',
                ],
                [
                    'attribute' => 'idMaquinas0.numero_de_serie',
                    'label' => 'Número de Serie',
                ],
                [
                    'attribute' => 'idEmpresas0.nombre',
                    'label' => 'Empresa',
                ],
                'tipo',
                [
                    'attribute' => 'descripcion',
                    'format' => 'ntext',
                    'value' => function ($model) {
                        return !empty($model->descripcion) ? $model->descripcion : '-';
                    }
                ],
                [
                    'attribute' => 'Fecha de inicio',
                    'value' => function($model) {
                        return Yii::$app->formatter->asDate($model->fecha_de_inicio);
                    },
                ],
                [
                    'attribute' => 'Fecha fin',
                    'value' => function($model) {
                        return Yii::$app->formatter->asDate($model->fecha_fin);
                    },
                ],
            ],
        ]) ?>
    </div>

</div>

<style>
    @media (min-width: 1200px) {
        .container, .container-sm, .container-md, .container-lg, .container-xl, .compras-create container, .compras-form card shadow-sm p-4 {
            max-width: 2540px;
        }
    }
    .container {
        padding-right: 0;
    }
    .content {
        background-color: #f8f9fa;
    }
    .summary {
        padding: 6px;
    }
    .contratos-view {
        background-color: #f8f9fa;
        padding: 20px;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .card .table thead th {
        background-color: #f1f1f1;
        border-bottom: 2px solid #dee2e6;
    }
    .card .table tbody tr:hover {
        background-color: #e9ecef;
    }
    .btn-primary {
        background-color: #0056b3;
        border-color: #0056b3;
    }
    .btn-primary:hover {
        background-color: #004494;
        border-color: #004494;
    }
    .table td, .table th {
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
        word-wrap: break-word;
        overflow-wrap: break-word;
    }
    .grid-view .action-column .btn {
        margin-right: 2px;
    }
    .contratos-view {
        background-color: #f8f9fa;
        padding: 20px;
        border-radius: 8px;
    }
    .btn-outline-secondary {
        border-color: #6c757d;
        color: #6c757d;
    }
    .btn-outline-secondary:hover {
        background-color: #6c757d;
        color: #fff;
    }
    .table td {
        max-width: 1000px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: normal;
    }
</style>
