<?php

use app\models\Contratos;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\search\ComprasSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Contratos';

?>
<div class="contratos-index container">

    <div class="d-flex justify-content-between align-items-center mb-3">
        <h1 class="mb-0"><?= Html::encode($this->title) ?></h1>
        <?= Html::a('Nueva Contrato', ['create'], ['class' => 'btn btn-primary']) ?>
    </div>
    <div class="input-group mb-3" style="width: 450px;">
        <input type="text" id="contratos-search" class="form-control table-search" style="width: 50px;" placeholder="Buscar...">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary btn-reset-search" type="button" id="reset-search">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>

    <div class="card shadow-sm p-4">
        <?php Pjax::begin(); ?>
        <div class="card-body p-0">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-hover mb-0', 'id' => 'contratos-table'],
                'columns' => [
                    [
                        'attribute' => 'idMaquinas',
                        'value' => 'idMaquinas0.nombre',
                        'label' => 'Máquina'
                    ],
                    [
                        'attribute' => 'idMaquinas',
                        'value' => 'idMaquinas0.numero_de_serie',
                        'label' => 'Número de Serie'
                    ],
                    [
                        'attribute' => 'idEmpresas',
                        'value' => 'idEmpresas0.nombre',
                        'label' => 'Empresa'
                    ],
                    'tipo',
                    [
                        'attribute' => 'descripcion',
                        'format' => 'ntext',
                        'value' => function ($model) {
                            return !empty($model->descripcion) ? $model->descripcion : '-';
                        }
                    ],
                    [
                        'attribute' => 'fecha_de_inicio',
                        'format' => ['date', 'php:d-m-Y'],
                        'contentOptions' => ['style' => 'white-space: nowrap;'],
                    ],
                    [
                        'attribute' => 'fecha_fin',
                        'format' => ['date', 'php:d-m-Y'],
                        'contentOptions' => ['style' => 'white-space: nowrap;'],
                    ],
                    [
                        'class' => ActionColumn::className(),
                        'urlCreator' => function ($action, Contratos $model, $key, $index, $column) {
                            return Url::toRoute([$action, 'id' => $model->id]);
                        },
                        'buttonOptions' => ['class' => 'btn btn-sm btn-outline-secondary mr-2'],
                        'contentOptions' => ['class' => 'text-right'], // Alineación a la derecha
                        'template' => '{view} {update} {delete} <div>{export-pdf}</div>',
                        'buttons' => [
                            'export-pdf' => function ($url, $model, $key) {
                                return Html::a('<i class="fas fa-file-pdf"></i>', '#', [
                                    'class' => 'btn btn-outline-secondary mt-2', // Quitamos la clase btn-sm
                                    'title' => 'Exportar a PDF',
                                    'data-pjax' => '0',
                                    'style' => 'width: 120px;margin-right: 8px;',
                                    'onclick' => 'window.open("' . Url::to(['contratos/export-pdf', 'id' => $model->id]) . '", "_blank"); return false;',
                                ]);
                            },
                        ],
                    ],



                ],
            ]); ?>
        </div>
        <?php Pjax::end(); ?>
    </div>
</div>

<style>
    @media (min-width: 1200px) {
        .container, .container-sm, .container-md, .container-lg, .container-xl {
            max-width: 3840px;
        }
    }
    
    .container {
        padding-right: 0;
    }
    .table-search {
        border: none;
        border-radius: 8px;
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
        padding: 8px 12px;
    }
    .content {
        background-color: #f8f9fa;
    }
    .summary {
        padding: 6px;
    }
    .contratos-index {
        background-color: #f8f9fa;
        padding: 20px;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .btn-reset-search {
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
    }
    .card .table thead th {
        background-color: #f1f1f1;
        border-bottom: 2px solid #dee2e6;
    }
    .card .table tbody tr:hover {
        background-color: #e9ecef;
    }
    .btn-primary {
        background-color: #0056b3;
        border-color: #0056b3;
    }
    .btn-primary:hover {
        background-color: #004494;
        border-color: #004494;
    }
    .table td, .table th {
        white-space: normal; /* Permite el salto de línea */
        text-overflow: ellipsis; /* Agrega puntos suspensivos al texto que se desborda */
        overflow: hidden; /* Oculta el texto que se desborda de la celda */
        word-wrap: break-word; /* Permite que las palabras largas se dividan y ajusten */
    }
    .grid-view .action-column .btn {
        margin-right: 2px;
    }
    .btn-outline-secondary {
        border-color: #6c757d;
        color: #6c757d;
    }
    .btn-outline-secondary:hover {
        background-color: #6c757d;
        color: #fff;
    }
    div.contratos-index input.table-search {
        width: 50px;
    }
    .table td {
        max-width: 200px; /* Define un ancho máximo para las celdas */
        overflow: hidden; /* Oculta el texto que se desborda de la celda */
        text-overflow: ellipsis; /* Agrega puntos suspensivos (...) al texto que se desborda */
        white-space: normal; /* Permite el salto de línea */
    }
</style>

<?php
$this->registerJs("
    document.getElementById('contratos-search').addEventListener('keyup', function() {
        var input, filter, table, tr, td, i, j, txtValue;
        input = document.getElementById('contratos-search');
        filter = input.value.toUpperCase();
        table = document.getElementById('contratos-table');
        tr = table.getElementsByTagName('tr');
        for (i = 1; i < tr.length; i++) {
            tr[i].style.display = 'none';
            td = tr[i].getElementsByTagName('td');
            for (j = 0; j < td.length; j++) {
                if (td[j]) {
                    txtValue = td[j].textContent || td[j].innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = '';
                        break;
                    }
                }
            }
        }
    });
    // Función para resetear el input de búsqueda y los resultados
    document.getElementById('reset-search').addEventListener('click', function() {
        document.getElementById('contratos-search').value = '';
        var table = document.getElementById('contratos-table');
        var tr = table.getElementsByTagName('tr');
        for (var i = 1; i < tr.length; i++) {
            tr[i].style.display = '';
        }
    });
");
?>

<?php
$this->registerJs("
    // Activar tooltips solo en las celdas con contenido truncado
    $('#compras-table td').each(function() {
        if (this.offsetWidth < this.scrollWidth) {
            $(this).attr('title', $(this).text());
            $(this).tooltip();
        }
    });
");
?>
