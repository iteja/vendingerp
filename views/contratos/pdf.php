<?php
use yii\helpers\Html;
$formatter = new \IntlDateFormatter('es_ES', \IntlDateFormatter::LONG, \IntlDateFormatter::NONE);
?>
<!DOCTYPE html>
<html>
<head>
    <title>Contrato de Instalación de Máquina Vending</title>
     <style>
        body { font-family: Arial, sans-serif; line-height: 1.6; margin: 20px; }
        .contract-title { text-align: center; font-size: 24px; font-weight: bold; margin-bottom: 20px; }
        .contract-section { margin-bottom: 20px; }
        .contract-section-title { font-size: 18px; font-weight: bold; margin-bottom: 10px; }
        .contract-paragraph { margin-bottom: 10px; }
        .contract-signature { margin-top: 50px; }
        .contract-signature div { margin-bottom: 40px; }
        table { width: 100%; border-collapse: collapse; margin-bottom: 20px; }
        table, th, td { border: 1px solid black; }
        th, td { padding: 8px; text-align: left; }
        .table td:nth-child(2) { /* Aplica estilos solo a la segunda columna */
            max-width: 300px; /* Define un ancho máximo para las celdas */
            word-wrap: break-word; /* Permite el salto de línea incluso en palabras largas */
            overflow-wrap: break-word; /* Permite el salto de línea incluso en palabras largas para navegadores que no soportan word-wrap */
        }
    </style>
</head>
<body>
    <div class="contract-title">Contrato de Instalación de Máquina Vending</div>

    <div class="contract-section">
        <div class="contract-section-title">1. PARTES</div>
        <div class="contract-paragraph">
            Este contrato es celebrado entre <strong><?= Html::encode($model->idEmpresas0->nombre) ?></strong>, con domicilio en <strong><?= Html::encode($model->idEmpresas0->direccion) ?></strong>, en la ciudad de <strong><?= Html::encode($model->idEmpresas0->ciudad) ?></strong>, representada por <strong><?= Html::encode($model->idEmpresas0->representante) ?></strong>, en adelante "La Empresa", y <strong><?= Html::encode($model->idMaquinas0->nombre) ?></strong>, representada por <strong>Iker Teja</strong>, en adelante "El Propietario".
        </div>
    </div>

    <div class="contract-section">
        <div class="contract-section-title">2. OBJETO</div>
        <div class="contract-paragraph">
            El objeto de este contrato es la instalación de una máquina vending, con número de serie <strong><?= Html::encode($model->idMaquinas0->numero_de_serie) ?></strong>, propiedad de "El Propietario" en las instalaciones de "La Empresa".
        </div>
    </div>

    <div class="contract-section">
        <div class="contract-section-title">3. DURACIÓN</div>
        <div class="contract-paragraph">
            Este contrato tendrá una duración de <strong><?= Html::encode($model->getDuracion()) ?></strong>, iniciando el <strong><?= Html::encode((new \DateTime($model->fecha_de_inicio))->format('d-m-Y')) ?></strong> y finalizando el <strong><?= Html::encode((new \DateTime($model->fecha_fin))->format('d-m-Y')) ?></strong>.
        </div>
    </div>

    <div class="contract-section">
        <div class="contract-section-title">4. OBLIGACIONES</div>
        <div class="contract-paragraph">
            <strong>4.1 Obligaciones de "El Propietario":</strong>
            <ul>
                <li>Instalar y mantener la máquina vending en buen estado de funcionamiento.</li>
                <li>Reponer los productos de la máquina vending de manera regular.</li>
                <li>Asumir los costos de mantenimiento y reparación de la máquina vending.</li>
            </ul>
        </div>
        <div class="contract-paragraph">
            <strong>4.2 Obligaciones de "La Empresa":</strong>
            <ul>
                <li>Proveer un espacio adecuado para la instalación de la máquina vending.</li>
                <li>Permitir el acceso de "El Propietario" para la reposición y mantenimiento de la máquina.</li>
                <li>No trasladar ni alterar la ubicación de la máquina vending sin consentimiento de "El Propietario".</li>
            </ul>
        </div>
    </div>

    <div class="contract-section">
        <div class="contract-section-title">5. PROPIEDAD DE INGRESOS</div>
        <div class="contract-paragraph">
            Todos los ingresos generados por la máquina vending serán propiedad de "El Propietario". "La Empresa" no tendrá derecho a una porción de los ingresos generados, salvo acuerdo específico escrito entre las partes.
        </div>
    </div>

    <div class="contract-section">
        <div class="contract-section-title">6. CONFIDENCIALIDAD</div>
        <div class="contract-paragraph">
            Ambas partes acuerdan mantener la confidencialidad de toda la información y datos relacionados con el presente contrato y no divulgar dicha información a terceros sin el consentimiento previo por escrito de la otra parte, excepto cuando sea requerido por ley.
        </div>
    </div>

    <div class="contract-section">
        <div class="contract-section-title">7. INFORMACIÓN DEL CONTRATO</div>
        <table>
            <tr>
                <th>Tipo de Contrato</th>
                <td><?= Html::encode($model->tipo) ?></td>
            </tr>
            <tr>
                <th>Descripción</th>
                <td><?= Html::encode($model->descripcion) ?></td>
            </tr>
        </table>
    </div>

    <div class="contract-section">
    <div class="contract-section-title">8. FIRMAS</div>
    <div class="contract-paragraph"></div>
    <div class="contract-paragraph"></div>
    <div class="contract-paragraph"></div>
    <div class="contract-paragraph">
        <strong>El Propietario:</strong> __________________________<br>
        Nombre: <?= Html::encode($model->idEmpresas0->nombre) ?><br>
        Fecha: 
    </div>
    <div class="contract-paragraph"></div>
    <div class="contract-paragraph"></div>
    <div class="contract-paragraph"></div>
    <div class="contract-paragraph"></div>
    <div class="contract-paragraph">
        <strong>La Empresa:</strong> __________________________<br>
        Nombre: <?= Html::encode($model->idMaquinas0->nombre) ?><br>
        Fecha: 
    </div>
</div>
    
</body>
</html>
