<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\models\Compras $model */

$this->title = 'Maquina ID: ' . $model->id;
\yii\web\YiiAsset::register($this);
?>

<div class="maquinas-view container">

    <div class="d-flex justify-content-between align-items-center mb-3">
        <h1 class="mb-0"><?= Html::encode($this->title) ?></h1>
        <?= Html::a('Volver a la lista', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <div class="card shadow-sm p-4">
        <div class="d-flex justify-content-end mb-3">
            <?= Html::a('Actualizar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary mr-2']) ?>
            <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => '¿Estás seguro de que deseas eliminar este elemento?',
                    'method' => 'post',
                ],
            ]) ?>
        </div>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'nombre',
                    'numero_de_serie',
                    [
                        'attribute' => 'tipo_de_maquina',
                        'value' => function ($model) {
                            return !empty($model->tipo_de_maquina) ? $model->tipo_de_maquina : '-';
                        }
                    ],
                    [
                        'label' => 'Número Incidencias',
                        'value' => function ($model) {
                            return $model->getTotalIncidences();
                        }
                    ],
                    [
                        'label' => 'Número Productos',
                        'value' => function ($model) {
                            return $model->getTotalProducts();
                        }
                    ],
//                    [
//                'attribute' => 'idProductos',
//                'format' => 'html',
//                'value' => function($model) {
//                    $productos = $model->idProductos;
//                    if (!empty($productos)) {
//                        $html = '';
//                        foreach ($productos as $producto) {
//                            $stock = $producto->getMaquinasProductos()->where(['idMaquinas' => $model->id])->one()->stock;
//                            $html .= '- ' . Html::encode($producto->nombre) . ' (Stock: ' . $stock . ')<br>';
//                        }
//                        return $html;
//                    }
//                    return '-';
//                },
//                'label' => 'Productos',
//            ],
            ],
        ]) ?>
    </div>

</div>

<style>
    @media (min-width: 1200px) {
        .container, .container-sm, .container-md, .container-lg, .container-xl, .compras-create container, .compras-form card shadow-sm p-4 {
            max-width: 2540px;
        }
    }
    .container {
        padding-right: 0;
    }
    .content {
        background-color: #f8f9fa;
    }
    .summary {
        padding: 6px;
    }
    .maquinas-view {
        background-color: #f8f9fa;
        padding: 20px;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .card .table thead th {
        background-color: #f1f1f1;
        border-bottom: 2px solid #dee2e6;
    }
    .card .table tbody tr:hover {
        background-color: #e9ecef;
    }
    .btn-primary {
        background-color: #0056b3;
        border-color: #0056b3;
    }
    .btn-primary:hover {
        background-color: #004494;
        border-color: #004494;
    }
    .table td, .table th {
        white-space: normal; /* Permite el salto de línea */
        text-overflow: ellipsis; /* Agrega puntos suspensivos al texto que se desborda */
        overflow: hidden; /* Oculta el texto que se desborda de la celda */
        word-wrap: break-word; /* Permite que las palabras largas se dividan y ajusten */
    }
    .grid-view .action-column .btn {
        margin-right: 2px;
    }
    .maquinas-view {
        background-color: #f8f9fa;
        padding: 20px;
        border-radius: 8px;
    }
    .btn-outline-secondary {
        border-color: #6c757d;
        color: #6c757d;
    }
    .btn-outline-secondary:hover {
        background-color: #6c757d;
        color: #fff;
    }
    .table td {
        max-width: 1000px; /* Define un ancho máximo para las celdas */
        overflow: hidden; /* Oculta el texto que se desborda de la celda */
        text-overflow: ellipsis; /* Agrega puntos suspensivos (...) al texto que se desborda */
        white-space: normal; /* Permite el salto de línea */
    }
</style>
