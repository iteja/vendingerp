

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Productos;


/** @var yii\web\View $this */
/** @var app\models\Maquinas $model */
/** @var yii\widgets\ActiveForm $form */

?>

<div class="maquinas-form card shadow-sm p-4">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'nombre')->label('Nombre*')->textInput(['maxlength' => true, 'placeholder' => 'Ej: Expendedora de café Espresso']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'numero_de_serie')->label('Número de serie*')->textInput(['maxlength' => true, 'placeholder' => 'Ej: 14R784EU712']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
           <?= $form->field($model, 'tipo_de_maquina')->textInput(['maxlength' => true, 'placeholder' => 'Ej: Máquina de café']) ?>
        </div>
    </div>

    
   
    <div class="form-group text-right">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
document.addEventListener('DOMContentLoaded', function() {
    let productoIndex = <?= count($model->maquinasProductos) ?>;
    document.getElementById('add-producto').addEventListener('click', function() {
        let container = document.getElementById('productos-container');
        let row = document.createElement('div');
        row.className = 'row producto-item';
        row.innerHTML = `
            <div class="col-md-6">
                <select name="Maquinas[maquinasProductosList][${productoIndex}][idProductos]" class="form-control">
                    <?php foreach (Productos::find()->all() as $producto): ?>
                        <option value="<?= $producto->id ?>"><?= $producto->nombre ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="col-md-4">
                <input type="text" name="Maquinas[maquinasProductosList][${productoIndex}][stock]" class="form-control">
            </div>
            <div class="col-md-2">
                <button type="button" class="remove-producto btn btn-danger">Eliminar</button>
            </div>
        `;
        container.appendChild(row);
        productoIndex++;
    });

    document.getElementById('productos-container').addEventListener('click', function(event) {
        if (event.target.classList.contains('remove-producto')) {
            event.target.closest('.producto-item').remove();
        }
    });
});
</script>

<style>
    .maquinas-form .form-group {
        margin-bottom: 1rem;
    }
    .maquinas-form .form-control {
        border-radius: 0.25rem;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .help-block {
        color: red;
        padding-left: 3px;
    }
    /* Alineación del botón */
    .maquinas-form .text-right .btn {
        margin-top: 10px; /* Espacio superior */
    }
    .producto-item {
        margin-bottom: 10px;
    }
</style>
