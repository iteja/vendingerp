<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/** @var yii\web\View $this */
/** @var app\models\Compras $model */
/** @var yii\widgets\ActiveForm $form */
/** @var array $piezaValues */

?>

<div class="incidencias-form card shadow-sm p-4">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'idMaquinas')->label('Máquina*')->dropDownList(
                ArrayHelper::map(\app\models\Maquinas::find()->all(), 'id', function($maquina) {
                    return $maquina->nombre . ' - ' . $maquina->numero_de_serie;
                }), 
                ['prompt' => 'Seleccione una Máquina']
            ) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'descripcion')->label('Descripción')->textarea(['maxlength' => true, 'rows' => 1, 'placeholder' => 'Ej: Avería en el dispensador de billetes']) ?>
        </div>
    </div>

    <div id="piezas-fecha-container" class="row">
        <div class="col-md-6">
            <label>Piezas Reemplazadas</label>
            <div id="piezas-container">
                <?php if (isset($piezaValues) && !empty($piezaValues)): ?>
                    <?php foreach ($piezaValues as $index => $pieza): ?>
                        <div class="form-group d-flex align-items-center">
                            <input type="text" class="form-control" name="PiezasReemplazadas[]" value="<?= Html::encode($pieza) ?>" placeholder="Pieza <?= $index + 1 ?>" maxlength="50">
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <div class="form-group d-flex align-items-center">
                        <input type="text" class="form-control" name="PiezasReemplazadas[]" placeholder="Pieza 1" maxlength="50">
                    </div>
                <?php endif; ?>
            </div>
            <button type="button" id="add-pieza" class="btn btn-primary btn-sm mb-2">Añadir Pieza</button>
            <button type="button" id="remove-last-pieza" class="btn btn-danger btn-sm mb-2">Eliminar Última Pieza</button>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'fecha')->label('Fecha*')->input('date', ['max' => date('Y-m-d')]) ?>
        </div>
    </div>

    <div class="form-group text-right mt-3">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<style>
    .incidencias-form .form-group {
        margin-bottom: 1rem;
    }
    .incidencias-form .form-control {
        border-radius: 0.25rem;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .help-block {
        color: red;
        padding-left: 3px;
    }
    #piezas-container .form-group {
        display: flex;
        align-items: center;
    }
    #piezas-container .form-group input {
        flex-grow: 1;
        margin-right: 10px;
    }
    .btn-sm {
        margin-bottom: 10px;
    }
</style>

<?php
$this->registerJs("
    var piezaIndex = " . (isset($piezaValues) && !empty($piezaValues) ? count($piezaValues) + 1 : 2) . ";

    $('#add-pieza').click(function() {
        $('#piezas-container').append(
            '<div class=\"form-group d-flex align-items-center\">' +
                '<input type=\"text\" class=\"form-control\" name=\"PiezasReemplazadas[]\" placeholder=\"Pieza ' + piezaIndex + '\" maxlength=\"50\">' +
            '</div>'
        );
        piezaIndex++;
    });

    $('#remove-last-pieza').click(function() {
        var piezas = $('#piezas-container .form-group');
        if (piezas.length > 1) {
            piezas.last().remove();
            piezaIndex--;
        }
    });
");
?>
