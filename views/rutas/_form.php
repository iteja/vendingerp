<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Empresas; // Asegúrate de importar el modelo Empresas

/** @var yii\web\View $this */
/** @var app\models\Rutas $model */
/** @var yii\widgets\ActiveForm $form */
/** @var app\models\Productos[] $productos */

$empresas = Empresas::find()->all();
$empresasList = ArrayHelper::map($empresas, 'id', 'nombre'); // Cambia 'nombre' al atributo adecuado de tu modelo

?>

<div class="rutas-form card shadow-sm p-4">

    <?php $form = ActiveForm::begin(['options' => ['class' => 'form-horizontal']]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'nombre')->label('Nombre de la Ruta*')->textInput(['maxlength' => true, 'placeholder' => 'Ej: Ruta 1']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'descripcion')->label('Descripción*')->textarea(['maxlength' => true, 'rows' => 1, 'placeholder' => 'Ej: Ruta de Santander a Torrelavega']) ?>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'empresasIds')->listBox($empresasList, [
                'multiple' => true,
                'size' => 12, // Ajusta el tamaño según tus necesidades
                'class' => 'form-control',
            ])->label('Empresas Participantes*') ?>
        </div>
    </div>

    <div class="form-group text-right">
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script>
    document.addEventListener('DOMContentLoaded', function() {
        const listBox = document.querySelector('.rutas-form .form-control[multiple]');
        listBox.addEventListener('click', function(e) {
            if (e.target.tagName === 'OPTION') {
                e.target.selected = !e.target.selected;
            }
        });

        // Evitar la selección múltiple predeterminada con arrastre
        listBox.addEventListener('mousedown', function(e) {
            e.preventDefault();
        });
    });
</script>

<style>
    .rutas-form .form-group {
        margin-bottom: 1rem;
    }
    .rutas-form .form-control {
        border-radius: 0.25rem;
    }
    .card {
        border: none;
        border-radius: 8px;
    }
    .help-block {
        color: red;
        padding-left: 3px;
    }
    /* Alineación del botón */
    .rutas-form .text-right .btn {
        margin-top: 10px; /* Espacio superior */
    }
    /* Estilo para el listBox */
    .rutas-form .form-control[multiple] {
        height: auto;
        min-height: 200px;
        border-radius: 0.25rem;
    }
</style>
