<?php
use yii\helpers\Html;
use yii\helpers\Json;

/* @var $this yii\web\View */
/* @var $ruta app\models\Rutas */
/* @var $empresas app\models\Empresa[] */

$this->title = 'Mapa de Ruta: ' . $ruta->nombre;

$empresasJson = Json::encode($empresas);
?>

<div class="d-flex justify-content-between align-items-center mb-3">
    <div>
        <h1 class="break-word"><?= Html::encode($this->title) ?></h1>
        <!--<p class="break-word"><?= Html::encode($ruta->descripcion) ?></p>-->
    </div>
    <?= Html::a('Volver a la lista', ['index'], ['class' => 'btn btn-outline-secondary']) ?>
</div>

<div id="map" class="map-container"></div>

<style>
    @media (min-width: 1200px) {
        .container, .container-sm, .container-md, .container-lg, .container-xl {
            max-width: 2540px;
        }
    }
    .container {
        padding-right: 20px;
        padding-left: 20px;
    }
    .table-search {
        border: none;
        border-radius: 8px;
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
        padding: 8px 12px;
    }
    .content {
        background-color: #f8f9fa;
    }
    .summary {
        padding: 6px;
    }
    .map-container {
        height: 60vh;
        width: 100%;
        border: 1px solid #dee2e6;
        border-radius: 8px;
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1);
    }
    .btn-primary {
        background-color: #0056b3;
        border-color: #0056b3;
    }
    .btn-primary:hover {
        background-color: #004494;
        border-color: #004494;
    }
    .table td, .table th {
        white-space: normal;
        text-overflow: ellipsis;
        overflow: hidden;
        word-wrap: break-word;
    }
    .grid-view .action-column .btn {
        margin-right: 2px;
    }
    .btn-outline-secondary {
        border-color: #6c757d;
        color: #6c757d;
    }
    .btn-outline-secondary:hover {
        background-color: #6c757d;
        color: #fff;
    }

    /* Ocultar controles de enrutamiento de Leaflet Routing Machine */
    .leaflet-routing-container,
    .leaflet-bar.leaflet-routing-container,
    .leaflet-control.leaflet-routing-container {
        display: none !important;
    }

    /* Estilo para las etiquetas personalizadas */
    .label {
        color: blue;
        font-weight: bold;
        text-align: center;
        font-size: 12px;
    }

    /* Estilo para permitir corte de palabras y ajuste en múltiples líneas */
    .break-word {
        overflow-wrap: break-word;
        word-break: break-all;
        white-space: normal;
    }

    /* Hacer que el mapa se ajuste automáticamente tanto al ancho como al largo */
    #map {
        width: 100%;
        height: calc(100vh - 200px); /* Ajusta la altura relativa a la altura de la ventana, restando un espacio para otros elementos */
        max-height: 1000px; /* Altura máxima */
    }

    body {
        overflow: hidden; /* Evita el scroll vertical */
    }
</style>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css" />
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>
    <script src="https://unpkg.com/leaflet-routing-machine/dist/leaflet-routing-machine.js"></script>
    <script src="https://unpkg.com/leaflet-control-geocoder/dist/Control.Geocoder.js"></script>
</head>
<body>
    <div id="map"></div>
    <div id="empresa-info" style="margin-top: 20px; font-size: 18px;"></div>

    <script>
    document.addEventListener("DOMContentLoaded", function() {
        var empresas = <?= $empresasJson ?>;
        var map = L.map('map').setView([0, 0], 2);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        var markers = [];
        var bounds = L.latLngBounds();

        empresas.forEach(function(empresa) {
            var coords = empresa.coordenadas.split(',');
            var latLng = L.latLng(parseFloat(coords[0]), parseFloat(coords[1]));
            var marker = L.marker(latLng).addTo(map);
            var label = L.divIcon({
                className: 'label',
                html: "<b>" + empresa.nombre + "</b>",
                iconSize: [100, 40],
                iconAnchor: [50, -20]  // Ajusta la posición de la etiqueta
            });

            L.marker(latLng, { icon: label }).addTo(map);

            markers.push(marker);
            bounds.extend(latLng);
        });

        map.fitBounds(bounds);

        // Configuración de la ruta
        var waypoints = markers.map(function(marker) {
            return marker.getLatLng();
        });

        L.Routing.control({
            waypoints: waypoints,
            routeWhileDragging: true,
            geocoder: L.Control.Geocoder.nominatim(),
            router: L.Routing.osrmv1({
                language: 'es',
                profile: 'driving-car'
            }),
            createMarker: function(i, waypoint) {
                return L.marker(waypoint.latLng);
            }
        }).addTo(map);
    });
    </script>
</body>
</html>
